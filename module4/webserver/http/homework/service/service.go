package service

import "io"

type Service interface {
	GetUsers() ([]byte, error)
	GetUser(id string) ([]byte, error)
	AddUser(userIO io.ReadCloser) error
}
