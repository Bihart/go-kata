package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/gerzhod/go-kata/module4/webserver/http/homework/model"
	"gitlab.com/gerzhod/go-kata/module4/webserver/http/homework/repo"
	"gitlab.com/gerzhod/go-kata/module4/webserver/http/homework/service"

	"github.com/go-chi/chi/v5/middleware"

	"github.com/go-chi/chi/v5"
)

func initLogger(mux *chi.Mux) *log.Logger {
	wrt, err := os.OpenFile("./module4/webserver/http/homework/log.log", os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		panic(fmt.Errorf("log file is not open \n %w", err))
	}

	//Реализуй механизм логирования запросов и ответов.
	logger := log.New(wrt, "", log.LstdFlags)
	handler := middleware.RequestLogger(&middleware.DefaultLogFormatter{Logger: logger, NoColor: true})
	if mux != nil {
		mux.Use(middleware.Logger, handler)
	}

	return logger
}

func initUserServise(logger *log.Logger) service.Service {
	return service.UserServ{Loger: logger, Repository: &repo.UserRepo{Loger: logger, Users: []model.User{}}}
}

func main() {
	r := chi.NewRouter()
	logger := initLogger(r)
	userServ := initUserServise(logger)
	srv := &http.Server{Addr: ":30009", Handler: r}

	//'/' — отображает приветственное сообщение в формате JSON;
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		j, err := json.Marshal("Hello, is a go-kata")
		if err != nil {
			WriteErr(err, w)
		}
		_, err = w.Write(j)
		if err != nil {
			WriteErr(err, w)
		}
	})

	//'/users' — отображает список пользователей в формате JSON;
	r.Route("/users", func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			users, err := userServ.GetUsers()
			if err != nil {
				WriteErr(err, w)
			}
			_, err = w.Write(users)
			if err != nil {
				WriteErr(err, w)
			}
		})

		//'/users/:id' — отображает информацию о конкретном пользователе в формате JSON;
		r.Get("/{ID}", func(w http.ResponseWriter, r *http.Request) {
			id := chi.URLParam(r, "ID")
			user, err := userServ.GetUser(id)
			if err != nil {
				WriteErr(err, w)
			}
			_, err = w.Write(user)
			if err != nil {
				WriteErr(err, w)
			}
		})

		//Реализуй обработку get- и post-запросов для маршрута '/users'.
		r.Post("/", func(w http.ResponseWriter, r *http.Request) {
			err := userServ.AddUser(r.Body)
			if err != nil {
				WriteErr(err, w)
			}
		})
	})

	//'/upload' — загружает любой файл с сохранением его названия в папку public;
	r.Post("/upload", func(w http.ResponseWriter, r *http.Request) {
		if r.MultipartForm == nil {
			err := r.ParseMultipartForm(32 << 20)
			if err != nil {
				WriteErr(err, w)
			}
		}

		if r.MultipartForm != nil && r.MultipartForm.File != nil {
			for fhs := range r.MultipartForm.File {
				mulFile, header, err := r.FormFile(fhs)
				if err != nil {
					WriteErr(err, w)
				}

				file, err := os.Create(fmt.Sprintf("./module4/webserver/http/homework/public/%s", header.Filename))
				if err != nil {
					WriteErr(err, w)
				}

				_, err = io.Copy(file, mulFile)
				if err != nil {
					WriteErr(err, w)
				}

				err = mulFile.Close()
				if err != nil {
					WriteErr(err, w)
				}
				err = file.Close()
				if err != nil {
					WriteErr(err, w)
				}
			}
		}

	})

	//'/public/filename.txt' — получение файлов из папки public.
	r.Get("/public/{filename}", func(w http.ResponseWriter, r *http.Request) {
		filename := chi.URLParam(r, "filename")

		file, err := os.Open(fmt.Sprintf("./module4/webserver/http/homework/public/%s", filename))
		if err != nil {
			WriteErr(err, w)
		}

		_, err = io.Copy(w, file)
		if err != nil {
			WriteErr(err, w)
		}

		err = file.Close()
		if err != nil {
			WriteErr(err, w)
		}
	})

	//Реализуй механизм graceful shutdown для корректной остановки сервера.
	go func() {
		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			panic(err)
		}

	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancelCnt := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelCnt()
	err := srv.Shutdown(ctx)
	if err != nil {
		panic(err)
	}

}

func WriteErr(err error, w http.ResponseWriter) {

	w.WriteHeader(500)
	_, err = w.Write([]byte(err.Error()))
	if err != nil {
		panic(err)
	}
}
