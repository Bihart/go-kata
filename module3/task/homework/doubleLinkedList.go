package main

//Реализуй загрузку данных в double linked list через метод LoadData, где path — это путь до test.json .
//
//Делая загрузку данных при постройке листа, отсортируй данные самописным QuickSort по дате
//
//Допиши в интерфейс LinkedLister недостающие методы.
//
//Реализуй все методы в DoubleLinkedList.
//
//Напиши тесты и бенчмарки к методам.
import (
	"encoding/json"
	"fmt"
	"io/fs"
	"math/rand"
	"os"
	"time"

	"github.com/brianvoe/gofakeit/v6"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error

	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

func QuickSort(commits []Commit) []Commit {
	if len(commits) <= 1 {
		return commits
	}
	median := commits[rand.Intn(len(commits))]

	before := make([]Commit, 0, len(commits))
	equal := make([]Commit, 0, len(commits))
	after := make([]Commit, 0, len(commits))

	for _, item := range commits {
		switch {
		case item.Date.Before(median.Date):
			before = append(before, item)
		case item.Date.Equal(median.Date):
			equal = append(equal, item)
		case item.Date.After(median.Date):
			after = append(after, item)
		}
	}
	before = QuickSort(before)
	after = QuickSort(after)

	before = append(before, equal...)
	before = append(before, after...)
	return before
}

func initLinkedLister() LinkedLister { //nolint:all
	return &DoubleLinkedList{len: -1}

}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	file, err := os.ReadFile(path)
	if err != nil {
		return fmt.Errorf("файл не найден %w", err)
	}

	var commits []Commit
	err = json.Unmarshal(file, &commits)
	if err != nil {
		panic(fmt.Errorf("не удалось Unmarshal json %w", err))
	}
	// отсортировать список используя самописный QuickSort
	commits = QuickSort(commits)

	for _, commit := range commits {
		err := d.Insert(d.len, commit)
		if err != nil {
			return err
		}
	}
	return nil
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
	//panic("implement me")
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.curr == nil {
		return nil
	} else if d.curr.next == nil {
		return nil
	}
	d.curr = d.curr.next
	return d.curr
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr == nil {
		return nil
	} else if d.curr.next == nil {
		return nil
	}
	d.curr = d.curr.prev
	return d.curr
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if n > d.len {
		return fmt.Errorf("n больше длинны массива")
	} else if d.len < 0 {
		node := &Node{data: &c}
		d.head = node
		d.tail = node
		d.curr = node

		d.len = 0
		return nil
	} else if n == d.len {
		n2 := &Node{data: &c, prev: d.tail}
		d.tail.next = n2
		d.tail = n2

		d.len++
		return nil
	} else if n < 0 {
		n2 := &Node{data: &c, next: d.head}
		d.head.prev = n2
		d.head = n2

		d.len++
		return nil
	}
	node := d.head
	for i := 0; i < n; i++ {
		node = node.next
	}
	n2 := &Node{data: &c, next: node.next, prev: node}
	node.next.prev = n2
	node.next = n2
	d.len++
	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if n > d.len {
		return fmt.Errorf("n больше длинны массива")
	} else if n < 0 {
		return fmt.Errorf("n меньше 0")
	} else if d.len < 0 {
		return fmt.Errorf("n больше длинны массива")
	} else if n <= 0 {
		d.head = d.head.next
		d.head.prev = nil

		d.len--
		return nil
	} else if n == d.len {
		d.tail = d.tail.prev
		d.tail.next = nil

		d.len--
		return nil
	}
	node := d.head
	for i := 0; i < n; i++ {
		node = node.next
	}
	node.prev.next, node.next.prev = node.next, node.prev
	d.len--
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.curr == nil {
		return fmt.Errorf("текущий элемент равен nil")
	} else if d.curr == d.head {
		d.head = d.head.next
		d.head.prev = nil
		d.curr = d.head

		d.len--
		return nil
	} else if d.curr == d.tail {
		d.tail = d.tail.prev
		d.tail.next = nil
		d.curr = d.tail

		d.len--
		return nil
	}
	d.curr.prev.next = d.curr.next
	d.curr.next.prev = d.curr.prev

	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.curr == nil {
		return -1, fmt.Errorf("текущий элемент равен nil")
	}

	i := 0
	node := d.head
	for ; i < d.len; i++ {
		if d.curr == node {
			break
		}
		node = node.next
	}
	return i, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	//Метод pop() удаляет последний элемент из массива и возвращает его значение.
	if d.tail == nil {
		return nil
	}
	node := d.tail
	d.tail = node.prev
	d.tail.next = nil

	d.len--
	return node
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	//Метод shift удаляет элемент по нулевому индексу, сдвигает значения по последовательным индексам вниз,
	//	а затем возвращает удалённое значение.
	if d.head == nil {
		return nil
	}
	node := d.head
	d.head = node.next
	d.head.prev = nil

	d.len--
	return node
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	if d.head == nil {
		return nil
	}
	node := d.head
	for i := 0; i <= d.len; i++ {
		if node.data.UUID == uuID {
			break
		}
		node = node.next
	}
	d.curr = node
	return node
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	if d.head == nil {
		return nil
	}
	node := d.head
	for i := 0; i < d.len+1; i++ {
		if node.data.Message == message {
			break
		}
		node = node.next
	}
	d.curr = node
	return node
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	reverseDoubleLinkedList := DoubleLinkedList{len: d.len}
	if d.len < 0 {
		return &reverseDoubleLinkedList
	}
	node := d.tail
	reverseNode := &Node{}
	reverseDoubleLinkedList.head = reverseNode
	for i := 0; i < d.len; i++ {
		reverseNode.data = node.data
		node = node.prev
		reverseNode.next = &Node{prev: reverseNode}
		reverseNode = reverseNode.next
	}
	reverseNode.data = d.head.data
	reverseDoubleLinkedList.tail = reverseNode
	reverseDoubleLinkedList.curr = reverseDoubleLinkedList.head
	return &reverseDoubleLinkedList
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON() {
	// Дополнительное задание написать генератор данных
	// используя библиотеку gofakeit

	n := rand.Intn(100)
	commits := make([]Commit, n)

	for i := 0; i < n; i++ {
		commits[i] = Commit{
			Date:    gofakeit.Date(),
			Message: gofakeit.HackerPhrase(),
			UUID:    gofakeit.UUID(),
		}
	}

	b, err := json.Marshal(commits)
	if err != nil {
		panic(fmt.Errorf("неудалось Marshal %w", err))
	}

	err = os.WriteFile("module3/task/homework/test_test.json", b, fs.ModeIrregular)
	if err != nil {
		panic(fmt.Errorf("некоректный путь %w", err))
	}

}
