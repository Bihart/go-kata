package main

import (
	"math/rand"
	"reflect"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
)

func TestDoubleLinkedList_Insert(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		n int
		c Commit
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		len     int
	}{
		{name: "inserting the first Commit into an empty LinkedList", fields: fields{len: -1},
			args: args{n: -1, c: Commit{}}, wantErr: false, len: 0},
		{name: "inserting a commit in LinkedList at position 0", fields: func() fields {
			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo

			return fields{len: 2, head: &nodeOne, tail: &nodeThree}
		}(),
			args: args{n: 0, c: Commit{}}, wantErr: false, len: 3},
		{name: "inserting a commit in LinkedList at position -1", fields: func() fields {
			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo

			return fields{len: 2, head: &nodeOne, tail: &nodeThree}
		}(),
			args: args{n: -1, c: Commit{}}, wantErr: false, len: 3},
		{name: "inserting a commit in LinkedList at position 2", fields: func() fields {
			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo

			return fields{len: 2, head: &nodeOne, tail: &nodeThree}
		}(),
			args: args{n: 2, c: Commit{}}, wantErr: false, len: 3},
		{name: "inserting a commit in LinkedList at position 957", fields: func() fields {
			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo

			return fields{len: 2, head: &nodeOne, tail: &nodeThree}
		}(),
			args: args{n: 957, c: Commit{}}, wantErr: true, len: 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.Insert(tt.args.n, tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Insert() error = %v, wantErr %v", err, tt.wantErr)
			} else if err == nil {
				if d.len != tt.len {
					t.Errorf("Insert() len = %d, wantLen %d", d.len, tt.len)
				}
				desiredNode := d.head
				for i := 0; i < tt.args.n+1; i++ {
					desiredNode = desiredNode.next
				}
				if tt.args.c != *desiredNode.data {
					t.Errorf("Insert() commit = %#v, wantCommit %#v", tt.args.c, *desiredNode.data)
				}
			}
		})
	}
}

func TestDoubleLinkedList_Len(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{name: "1", fields: fields{len: 1}, want: 1},
		{name: "-1", fields: fields{len: -1}, want: -1},
		{name: "2", fields: fields{len: 2}, want: 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Len(); got != tt.want {
				t.Errorf("Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Current(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name:   "empty list",
			want:   nil,
			fields: fields{},
		},
		func() struct {
			name   string
			fields fields
			want   *Node
		} {

			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo

			return struct {
				name   string
				fields fields
				want   *Node
			}{
				name:   "completed list",
				fields: fields{len: 2, head: &nodeOne, tail: &nodeThree, curr: &nodeTwo},
				want:   &nodeTwo,
			}
		}(),
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Current(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Current() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Next(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name:   "empty list",
			fields: fields{},
			want:   nil,
		},
		func() struct {
			name   string
			fields fields
			want   *Node
		} {

			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo

			return struct {
				name   string
				fields fields
				want   *Node
			}{
				name:   "completed list",
				fields: fields{len: 2, head: &nodeOne, tail: &nodeThree, curr: &nodeTwo},
				want:   nodeTwo.next,
			}
		}(),
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Next(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Next() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Prev(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name:   "empty list",
			fields: fields{},
			want:   nil,
		},
		func() struct {
			name   string
			fields fields
			want   *Node
		} {

			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo

			return struct {
				name   string
				fields fields
				want   *Node
			}{
				name:   "completed list",
				fields: fields{len: 2, head: &nodeOne, tail: &nodeThree, curr: &nodeTwo},
				want:   nodeTwo.prev,
			}
		}(),
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Prev(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Prev() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Delete(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		n int
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantErr  bool
		wantLen  int
		dontWant *Node
	}{
		{name: "empty list", fields: fields{}, args: args{n: 1}, wantErr: true},
		{name: "empty args", fields: func() fields {
			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo

			return fields{len: 2, head: &nodeOne, tail: &nodeThree}
		}(), args: args{}, wantErr: false, wantLen: 1},
		func() struct {
			name     string
			fields   fields
			args     args
			wantErr  bool
			wantLen  int
			dontWant *Node
		} {
			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo
			return struct {
				name     string
				fields   fields
				args     args
				wantErr  bool
				wantLen  int
				dontWant *Node
			}{
				name:     "deleting an element",
				fields:   fields{len: 2, head: &nodeOne, tail: &nodeThree},
				args:     args{n: 1},
				wantErr:  false,
				wantLen:  1,
				dontWant: &nodeTwo}
		}(),
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.Delete(tt.args.n); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			} else if err == nil {
				if d.len != tt.wantLen {
					t.Errorf("Delete() len = %v, wantLen %v", d.len, tt.wantLen)
				}
				for i := 0; i < d.len; i++ {
					node := d.head
					for i := 0; i < d.len; i++ {
						if node == tt.dontWant {
							t.Errorf("Delete() node %#v should not be contained ", tt.dontWant)
						}
						node = node.next
					}
				}
			}
		})
	}
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name     string
		fields   fields
		wantErr  bool
		wantLen  int
		dontWant *Node
	}{
		{name: "empty list", fields: fields{}, wantErr: true},
		func() struct {
			name     string
			fields   fields
			wantErr  bool
			wantLen  int
			dontWant *Node
		} {
			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo
			return struct {
				name     string
				fields   fields
				wantErr  bool
				wantLen  int
				dontWant *Node
			}{
				name:     "deleting an element",
				fields:   fields{len: 2, head: &nodeOne, tail: &nodeThree, curr: &nodeTwo},
				wantErr:  false,
				wantLen:  1,
				dontWant: &nodeTwo}
		}(),
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.DeleteCurrent(); (err != nil) != tt.wantErr {
				t.Errorf("DeleteCurrent() error = %v, wantErr %v", err, tt.wantErr)
			} else if err == nil {
				if d.len != tt.wantLen {
					t.Errorf("Delete() len = %v, wantLen %v", d.len, tt.wantLen)
				}
				for i := 0; i < d.len; i++ {
					node := d.head
					for i := 0; i < d.len; i++ {
						if node == tt.dontWant {
							t.Errorf("Delete() node %#v should not be contained ", tt.dontWant)
						}
						node = node.next
					}
				}
			}
		})
	}
}

func TestDoubleLinkedList_Index(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name    string
		fields  fields
		want    int
		wantErr bool
	}{
		{name: "empty list", fields: fields{}, wantErr: true, want: -1},
		{name: "empty args", fields: func() fields {
			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo

			return fields{len: 2, head: &nodeOne, tail: &nodeThree, curr: &nodeOne}
		}(), wantErr: false, want: 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			got, err := d.Index()
			if (err != nil) != tt.wantErr {
				t.Errorf("Index() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Index() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Pop(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{name: "empty list", fields: fields{}, want: nil},
		func() struct {
			name   string
			fields fields
			want   *Node
		} {
			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo
			return struct {
				name   string
				fields fields
				want   *Node
			}{
				name:   "deleting an element",
				fields: fields{len: 2, head: &nodeOne, tail: &nodeThree, curr: &nodeTwo},
				want:   &nodeThree,
			}
		}(),
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Pop(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Pop() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Shift(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{

		{name: "empty list", fields: fields{}, want: nil},
		func() struct {
			name   string
			fields fields
			want   *Node
		} {
			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo
			return struct {
				name   string
				fields fields
				want   *Node
			}{
				name:   "deleting an element",
				fields: fields{len: 2, head: &nodeOne, tail: &nodeThree, curr: &nodeTwo},
				want:   &nodeOne,
			}
		}(),
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Shift(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Shift() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_SearchUUID(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		uuID string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Node
	}{
		{name: "empty list", fields: fields{}, want: nil},
		func() struct {
			name   string
			fields fields
			args   args
			want   *Node
		} {
			nodeOne := Node{data: &Commit{UUID: "e46702ae-dcfa-11ed-afa1-0242ac120001"}}
			nodeTwo := Node{data: &Commit{UUID: "e46702ae-dcfa-11ed-afa1-0242ac120002"}}
			nodeThree := Node{data: &Commit{UUID: "e46702ae-dcfa-11ed-afa1-0242ac120003"}}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo
			return struct {
				name   string
				fields fields
				args   args
				want   *Node
			}{
				name:   "deleting an element",
				fields: fields{len: 2, head: &nodeOne, tail: &nodeThree, curr: &nodeTwo},
				want:   nil,
			}
		}(),
		func() struct {
			name   string
			fields fields
			args   args
			want   *Node
		} {
			nodeOne := Node{data: &Commit{UUID: "e46702ae-dcfa-11ed-afa1-0242ac120001"}}
			nodeTwo := Node{data: &Commit{UUID: "e46702ae-dcfa-11ed-afa1-0242ac120002"}}
			nodeThree := Node{data: &Commit{UUID: "e46702ae-dcfa-11ed-afa1-0242ac120003"}}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo
			return struct {
				name   string
				fields fields
				args   args
				want   *Node
			}{
				name:   "deleting an element",
				fields: fields{len: 2, head: &nodeOne, tail: &nodeThree, curr: &nodeTwo},
				args:   args{uuID: "e46702ae-dcfa-11ed-afa1-0242ac120002"},
				want:   &nodeTwo,
			}
		}(),
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.SearchUUID(tt.args.uuID); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SearchUUID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Search(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		message string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Node
	}{
		{name: "empty list", fields: fields{}, want: nil},
		func() struct {
			name   string
			fields fields
			args   args
			want   *Node
		} {
			nodeOne := Node{data: &Commit{Message: "e46702a001"}}
			nodeTwo := Node{data: &Commit{Message: "e46702a002"}}
			nodeThree := Node{data: &Commit{Message: "e46702a003"}}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo
			return struct {
				name   string
				fields fields
				args   args
				want   *Node
			}{
				name:   "no args",
				fields: fields{len: 2, head: &nodeOne, tail: &nodeThree, curr: &nodeTwo},
				want:   nil,
			}
		}(),
		func() struct {
			name   string
			fields fields
			args   args
			want   *Node
		} {
			nodeOne := Node{data: &Commit{Message: "e46702a001"}}
			nodeTwo := Node{data: &Commit{Message: "e46702a002"}}
			nodeThree := Node{data: &Commit{Message: "e46702a003"}}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo
			return struct {
				name   string
				fields fields
				args   args
				want   *Node
			}{
				name:   "deleting an element",
				fields: fields{len: 2, head: &nodeOne, tail: &nodeThree, curr: &nodeTwo},
				args:   args{message: "e46702a002"},
				want:   &nodeTwo,
			}
		}(),
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Search(tt.args.message); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Search() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Reverse(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{name: "empty args", fields: func() fields {
			nodeOne := Node{}
			nodeTwo := Node{}
			nodeThree := Node{}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo

			return fields{len: 2, head: &nodeOne, tail: &nodeThree}
		}()},
		{name: "not empty args", fields: func() fields {
			nodeOne := Node{data: &Commit{Message: "e46702a001"}}
			nodeTwo := Node{data: &Commit{Message: "e46702a002"}}
			nodeThree := Node{data: &Commit{Message: "e46702a003"}}

			nodeOne.next = &nodeTwo

			nodeTwo.prev = &nodeOne
			nodeTwo.next = &nodeThree

			nodeThree.prev = &nodeTwo

			return fields{len: 2, head: &nodeOne, tail: &nodeThree}
		}()},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			newD := d.Reverse()
			if d.len != newD.len {
				t.Errorf("Reverse() len = %d, want %d", newD.len, d.len)
			} else {
				dLLNode := d.head
				reverseDLLNode := newD.tail
				for i := 0; i < d.len; i++ {
					if dLLNode.data != reverseDLLNode.data {
						t.Errorf("Reverse() data = %#v, want %#v", dLLNode.data, reverseDLLNode.data)
					} else {
						dLLNode = dLLNode.next
						reverseDLLNode = reverseDLLNode.prev
					}
				}
			}
		})
	}
}

func BenchmarkDoubleLinkedList_Insert(b *testing.B) {
	dLL := initLinkedLister()
	data := make([]Commit, 1000)
	for i := 0; i < 1000; i++ {
		data[i] = Commit{
			Date:    gofakeit.Date(),
			Message: gofakeit.HackerPhrase(),
			UUID:    gofakeit.UUID(),
		}
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for _, commit := range data {
			_ = dLL.Insert(dLL.Len(), commit)
		}
	}
}

func BenchmarkDoubleLinkedList_Scatter_Insert(b *testing.B) {
	dLL := initLinkedLister()
	data := make([]Commit, 1000)
	for i := 0; i < 1000; i++ {
		data[i] = Commit{
			Date:    gofakeit.Date(),
			Message: gofakeit.HackerPhrase(),
			UUID:    gofakeit.UUID(),
		}
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for _, commit := range data {
			_ = dLL.Insert(rand.Intn(dLL.Len()+2), commit)
		}
	}
}

func BenchmarkDoubleLinkedList_Scatter_Delete(b *testing.B) {
	dLL := initLinkedLister()
	for i := 0; i < 1000; i++ {
		_ = dLL.Insert(dLL.Len(), Commit{
			Date:    gofakeit.Date(),
			Message: gofakeit.HackerPhrase(),
			UUID:    gofakeit.UUID(),
		})
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for dLL.Len() < 0 {
			_ = dLL.Delete(rand.Intn(dLL.Len() + 2))
		}
	}
}

func BenchmarkDoubleLinkedList_Scatter_Search(b *testing.B) {
	dLL := initLinkedLister()
	for i := 0; i < 999; i++ {
		_ = dLL.Insert(dLL.Len(), Commit{
			Date:    gofakeit.Date(),
			Message: gofakeit.HackerPhrase(),
			UUID:    gofakeit.UUID(),
		})
	}
	message := gofakeit.HackerPhrase()
	_ = dLL.Insert(rand.Intn(999), Commit{
		Date:    gofakeit.Date(),
		Message: message,
		UUID:    gofakeit.UUID(),
	})

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for dLL.Len() < 0 {
			_ = dLL.Search(message)
		}
	}
}
