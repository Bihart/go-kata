package main

import "fmt"

func main() {
	conditioner := NewAirConditionerProxy(false)
	err := conditioner.TurnOnConditioner()
	if err != nil {
		fmt.Println(err)
	}
	err = conditioner.TurnOffConditioner()
	if err != nil {
		fmt.Println(err)
	}
	err = conditioner.SetTemperature(25.5)
	if err != nil {
		fmt.Println(err)
	}

	conditioner.authenticated = true
	err = conditioner.TurnOnConditioner()
	if err != nil {
		fmt.Println(err)
	}
	err = conditioner.TurnOffConditioner()
	if err != nil {
		fmt.Println(err)
	}
	err = conditioner.SetTemperature(18.14838)
	if err != nil {
		fmt.Println(err)
	}
}
