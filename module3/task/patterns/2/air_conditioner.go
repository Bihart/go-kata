package main

import "fmt"

type AirConditioner interface {
	TurnOnConditioner() error
	TurnOffConditioner() error
	SetTemperature(temperature float64) error
}

type RealAirConditioner struct {
	temperature float64
	isOn        bool
}

func (conditioner *RealAirConditioner) TurnOnConditioner() error {
	conditioner.isOn = true
	fmt.Println("Turn on the air conditioner")
	return nil
}

func (conditioner *RealAirConditioner) TurnOffConditioner() error {
	conditioner.isOn = false
	fmt.Println("Turn off the air conditioner")
	return nil
}

func (conditioner *RealAirConditioner) SetTemperature(temperature float64) error {
	conditioner.temperature = temperature
	fmt.Printf("Turn air conditioner temperature to %2.2f", temperature)
	return nil
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func (conditioner *AirConditionerAdapter) TurnOnConditioner() error {
	return conditioner.airConditioner.TurnOnConditioner()
}

func (conditioner *AirConditionerAdapter) TurnOffConditioner() error {
	return conditioner.airConditioner.TurnOffConditioner()
}

func (conditioner *AirConditionerAdapter) SetTemperature(temperature float64) error {
	return conditioner.airConditioner.SetTemperature(temperature)
}

type AirConditionerProxy struct {
	//Создайте структуру AirConditionerProxy, который представляет собой прокси-объект для AirConditionerAdapter.
	//Структура должна содержать поле adapter типа *AirConditionerAdapter и поле authenticated типа bool, которое
	//определяет, имеет ли пользователь право доступа к кондиционеру. Реализуйте методы интерфейса AirConditioner
	//для прокси-структуру, добавив проверку аутентификации перед выполнением операций с кондиционером.

	adapter       *AirConditionerAdapter
	authenticated bool
}

func (proxy *AirConditionerProxy) TurnOnConditioner() error {
	if proxy.authenticated {
		err := proxy.adapter.TurnOnConditioner()
		if err != nil {
			return err
		}
		return nil
	}
	return fmt.Errorf("Access denied: authentification required to turn on the air conditioner")
}

func (proxy *AirConditionerProxy) TurnOffConditioner() error {
	if proxy.authenticated {
		err := proxy.adapter.TurnOffConditioner()
		if err != nil {
			return err
		}
		return nil
	}
	return fmt.Errorf("Access denied: authentification required to turn off the air conditioner")
}

func (proxy *AirConditionerProxy) SetTemperature(temperature float64) error {
	if proxy.authenticated {
		err := proxy.adapter.SetTemperature(temperature)
		if err != nil {
			return err
		}
		return nil
	}
	return fmt.Errorf("Access denied: authentification required to set the temperature of the air conditioner")
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	return &AirConditionerProxy{
		authenticated: authenticated,
		adapter: &AirConditionerAdapter{
			airConditioner: &RealAirConditioner{
				temperature: 23.0,
				isOn:        false,
			},
		},
	}
}
