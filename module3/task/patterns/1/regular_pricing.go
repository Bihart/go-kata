package main

type RegularPricing struct {
}

func (receiver *RegularPricing) Calculate(order Order) float64 {
	//В структуре "RegularPricing" реализуй метод "Calculate", чтобы вернуть исходную цену заказа.
	return order.Quantity * order.Price
}
