package main

type SalePricing struct {
	discount float64
}

func (sale SalePricing) Calculate(order Order) float64 {
	//В структуре "SalePricing" реализуй метод "Calculate", чтобы вернуть скидочную цену заказа на основе
	//предоставленной скидки в процентах. В данной структуре должно присутствовать название скидочной стратегии,
	//например "весенняя скидка", "скидка регулярного клиента".
	return 0.01 * order.Quantity * order.Price * (100.0 - sale.discount)
}
