package main

type PricingStrategy interface {
	Calculate(order Order) float64
}

type Order struct {
	Product  string
	Price    float64
	Quantity float64
}
