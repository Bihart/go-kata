package main

import (
	"fmt"
	"math/rand"
)

func main() {
	pricing := make([]PricingStrategy, 15)

	for i := 0; i < len(pricing); i++ {
		if rand.Intn(2) > 0 {
			pricing[i] = &SalePricing{rand.Float64() + float64(rand.Intn(16))}
		} else {
			pricing[i] = &RegularPricing{}
		}
	}

	for i := 0; i < len(pricing); i++ {
		fmt.Printf(
			"Total cost with	 %#002.3v:	%6.2f\n",
			pricing[i],
			pricing[i].Calculate(
				Order{
					Price:    float64(rand.Intn(5000)),
					Quantity: float64(rand.Intn(100)),
				}))

	}
}
