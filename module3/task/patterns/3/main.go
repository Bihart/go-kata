package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

func readBody(response *http.Response) (bodyBytes []byte, err error) {
	defer func(Body io.ReadCloser) {
		err = Body.Close()
	}(response.Body)
	length := 1024
	n := length

	for n >= length {
		bytes := make([]byte, length)
		n, err = response.Body.Read(bytes)
		if err != nil && err != io.EOF {
			return
		}

		bytes = bytes[:n]
		bodyBytes = append(bodyBytes, bytes...)
	}
	return
}

type GeoCoordinates struct {
	Name       string      `json:"name"`
	LocalNames interface{} `json:"local_names"`
	Lat        float64     `json:"lat"`
	Lon        float64     `json:"lon"`
	Country    string      `json:"country"`
	State      string      `json:"state"`
}

type WeatherData struct {
	Coord struct {
		Lon float64 `json:"lon"`
		Lat float64 `json:"lat"`
	} `json:"coord"`
	Weather []struct {
		ID          int    `json:"id"`
		Main        string `json:"main"`
		Description string `json:"description"`
		Icon        string `json:"icon"`
	} `json:"weather"`
	Base string `json:"base"`
	Main struct {
		Temp      float64 `json:"temp"`
		FeelsLike float64 `json:"feels_like"`
		TempMin   float64 `json:"temp_min"`
		TempMax   float64 `json:"temp_max"`
		Pressure  int     `json:"pressure"`
		Humidity  int     `json:"humidity"`
	} `json:"main"`
	Visibility int `json:"visibility"`
	Wind       struct {
		Speed float64 `json:"speed"`
		Deg   int     `json:"deg"`
	} `json:"wind"`
	Clouds struct {
		All int `json:"all"`
	} `json:"clouds"`
	Dt  int `json:"dt"`
	Sys struct {
		Type    int    `json:"type"`
		ID      int    `json:"id"`
		Country string `json:"country"`
		Sunrise int    `json:"sunrise"`
		Sunset  int    `json:"sunset"`
	} `json:"sys"`
	Timezone int    `json:"timezone"`
	ID       int    `json:"id"`
	Name     string `json:"name"`
	Cod      int    `json:"cod"`
}

// WeatherAPI is the interface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature(data WeatherData) float64
	GetHumidity(data WeatherData) int
	GetWindSpeed(data WeatherData) float64
	GetLocation(location string) (GeoCoordinates, error)
	GetWeatherData(location GeoCoordinates) (WeatherData, error)
}

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey string
	client *http.Client
}

func (o OpenWeatherAPI) GetLocation(location string) (GeoCoordinates, error) {

	vvv := fmt.Sprintf("https://api.openweathermap.org/geo/1.0/direct?q=%s&appid=%s", location, o.apiKey)

	get, err := o.client.Get(vvv)
	if err != nil {
		return GeoCoordinates{}, err
	}
	if get.Status != "200 OK" {
		return GeoCoordinates{}, fmt.Errorf("failed to get location, response status %s", get.Status)
	}

	body, err := readBody(get)
	if err != nil {
		return GeoCoordinates{}, err
	}

	var geo []GeoCoordinates

	err = json.Unmarshal(body, &geo)
	if err != nil {
		return GeoCoordinates{}, err
	}

	if len(geo) < 1 {
		return GeoCoordinates{}, fmt.Errorf("location not found")
	}
	return geo[0], err

}

func (o OpenWeatherAPI) GetWeatherData(location GeoCoordinates) (WeatherData, error) {
	get, err := o.client.Get(fmt.Sprintf(
		"https://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=%s",
		location.Lat,
		location.Lon,
		o.apiKey,
	))
	if err != nil {
		return WeatherData{}, err
	}
	if get.Status != "200 OK" {
		return WeatherData{}, fmt.Errorf("failed to get Temperature, response status %s", get.Status)
	}

	body, err := readBody(get)
	if err != nil {
		return WeatherData{}, err
	}
	var data WeatherData

	err = json.Unmarshal(body, &data)
	if err != nil {
		return WeatherData{}, err
	}
	return data, nil
}

func (o *OpenWeatherAPI) GetTemperature(data WeatherData) float64 {
	return data.Main.Temp
}

func (o *OpenWeatherAPI) GetHumidity(data WeatherData) int {
	return data.Main.Humidity
}

func (o *OpenWeatherAPI) GetWindSpeed(data WeatherData) float64 {
	return data.Wind.Speed
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	temperature float64
	humidity    int
	windSpeed   float64
	weatherAPI  WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(locationStr string) error {
	location, err := w.weatherAPI.GetLocation(locationStr)
	if err != nil {
		return err
	}
	data, err := w.weatherAPI.GetWeatherData(location)
	if err != nil {
		return err
	}

	w.temperature = w.weatherAPI.GetTemperature(data)
	w.humidity = w.weatherAPI.GetHumidity(data)
	w.windSpeed = w.weatherAPI.GetWindSpeed(data)

	return nil
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{
			apiKey: apiKey,
			client: &http.Client{},
		},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("1e992bfe17c6d00bd248dcd001e458eb")
	cities := []string{"Москва", "Санкт-Петербуг", "Санкт-Петербург", "Казань", "Якутск"}

	for _, city := range cities {
		err := weatherFacade.GetWeatherInfo(city)
		if err != nil {
			fmt.Printf(""+city+": %v\n\n", err)
			continue
		}
		fmt.Printf("Temperature in "+city+": %f\n", weatherFacade.temperature)
		fmt.Printf("Humidity in "+city+": %d\n", weatherFacade.humidity)
		fmt.Printf("Wind speed in "+city+": %f\n\n", weatherFacade.windSpeed)
	}
}
