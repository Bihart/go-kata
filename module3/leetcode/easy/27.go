package main

func balancedStringSplit(s string) int { //nolint:all
	r := 0
	l := 0
	count := 0
	for i := 0; i < len(s); i++ {
		if s[i] == 'R' {
			r++
		}
		if s[i] == 'L' {
			l++
		}
		if r == l {
			count++
			r, l = 0, 0
		}
	}
	return count
}
