package main

func findKthPositive(arr []int, k int) int { //nolint:all
	i := 1
	for j := 0; k >= 1; i++ {
		if len(arr) > j && i == arr[j] {
			j++
		} else {
			k--
		}
	}
	return i - 1
}
