package main

func smallerNumbersThanCurrent(nums []int) []int { //nolint:all
	r := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		for j := 0; j < len(nums); j++ {
			if nums[i] > nums[j] {
				r[i]++
			}
		}
	}
	return r
}
