package main

import "sort"

func minimumSum(num int) int { //nolint:all
	nums := make([]int, 4)
	for i := 0; i < len(nums); i++ {
		nums[i] = num % 10
		num /= 10
	}

	sort.Ints(nums)

	return (nums[0]*10 + nums[1]*10) + nums[2] + nums[3]

}
