package main

type ParkingSystem struct {
	big    int
	medium int
	small  int
}

func Constructor(big int, medium int, small int) ParkingSystem { //nolint:all
	return ParkingSystem{big: big, medium: medium, small: small}
}

func (this *ParkingSystem) AddCar(carType int) bool { //nolint:all
	if carType == 2 {
		if this.medium <= 0 {
			return false
		} else {
			this.medium--
			return true
		}
	} else if carType > 2 {
		if this.small <= 0 {
			return false
		} else {
			this.small--
			return true
		}
	}
	if this.big <= 0 {
		return false
	}
	this.big--
	return true

}

/**
 * Your ParkingSystem object will be instantiated and called as such:
 * obj := Constructor(big, medium, small);
 * param_1 := obj.AddCar(carType);
 */
