package main

func decode(encoded []int, first int) []int { //nolint:all
	r := make([]int, len(encoded)+1)
	r[0] = first

	for i := 0; i < len(encoded); i++ {
		r[i+1] = encoded[i] ^ r[i]

	}
	return r
}
