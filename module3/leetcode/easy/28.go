package main

func countDigits(num int) int { //nolint:all
	count := 0
	numT := num

	for numT != 0 {
		if num%(numT%10) == 0 {
			count++
		}
		numT /= 10
	}
	return count
}
