package main

func runningSum(nums []int) []int { //nolint:all

	for i := 1; i < len(nums); i++ {
		nums[i] += nums[i-1]
	}
	return nums
}
