package main

func interpret(command string) string { //nolint:all
	var r []rune
	for i := 0; i < len(command); i++ {
		if command[i] == '(' {
			i++
			if command[i] == ')' {
				r = append(r, 'o')
			} else {
				for ; i < len(command); i++ {
					if command[i] == ')' {
						break
					}
					r = append(r, rune(command[i]))
				}
			}

		} else {
			r = append(r, rune(command[i]))
		}

	}
	return string(r)
}
