package main

//Дана нулевая перестановка nums (с нулевым индексом), создайте массив ans одинаковой длины,
//где ans[i] = nums[nums[i]] для каждого 0 <= i < nums.length и верните его.
//Нулевая перестановка nums - это массив уникальных целых чисел от 0 до nums.length - 1 (включительно).

func buildArray(nums []int) []int { //nolint:all
	ans := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		ans[i] = nums[nums[i]]
	}
	return ans
}
