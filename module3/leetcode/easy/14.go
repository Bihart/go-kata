package main

func maximumWealth(accounts [][]int) int { //nolint:all
	var max int
	for _, account := range accounts {
		v := 0
		for i := 0; i < len(account); i++ {
			v += account[i]
		}
		if v > max {
			max = v
		}
	}
	return max
}
