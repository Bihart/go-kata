package main

func shuffle(nums []int, n int) []int { //nolint:all
	ret := make([]int, len(nums))
	for i := 0; i < len(nums); i += 2 {
		ret[i] = nums[i/2]
		ret[i+1] = nums[n+(i/2)]
	}
	return ret
}
