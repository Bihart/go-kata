package main

func uniqueMorseRepresentations(words []string) int { //nolint:all
	morseCodeMap := map[rune]string{
		'a': ".-", 'b': "-...", 'c': "-.-.", 'd': "-..", 'e': ".", 'f': "..-.", 'g': "--.",
		'h': "....", 'i': "..", 'j': ".---", 'k': "-.-", 'l': ".-..", 'm': "--", 'n': "-.",
		'o': "---", 'p': ".--.", 'q': "--.-", 'r': ".-.", 's': "...", 't': "-", 'u': "..-",
		'v': "...-", 'w': ".--", 'x': "-..-", 'y': "-.--", 'z': "--.."}

	var ret []string

	for _, word := range words {
		var morseWord []rune
		for _, r := range word {
			for _, r2 := range morseCodeMap[r] {
				morseWord = append(morseWord, r2)
			}
		}
		t := true
		for _, s := range ret {
			if string(morseWord) == s {
				t = false
				break
			}
		}
		if t {
			ret = append(ret, string(morseWord))
		}
	}

	return len(ret)
}
