package main

// 1. N-th Tribonacci Number
func tribonacci(n int) int { //nolint:all
	if n < 1 {
		return 0
	}
	var tribonacci = [3]int{0, 0, 1}

	for i := 0; i < n-2; i++ {
		tribonacci[i-(i/3)*3] = tribonacci[0] + tribonacci[1] + tribonacci[2]

	}
	return tribonacci[0] + tribonacci[1] + tribonacci[2]
}
