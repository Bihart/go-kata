package main

func xorOperation(n int, start int) int { //nolint:all
	r := start
	for i := 0; i < n-1; i++ {
		start += 2
		r = r ^ start
	}
	return r
}
