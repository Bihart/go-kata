package main

func numJewelsInStones(jewels string, stones string) int { //nolint:all
	var count int
	for i := 0; i < len(jewels); i++ {
		for j := 0; j < len(stones); j++ {
			if jewels[i] == stones[j] {
				count++
			}
		}
	}
	return count
}
