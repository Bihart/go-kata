package main

import "strings"

func mostWordsFound(sentences []string) int { //nolint:all
	max := 0
	for _, sentence := range sentences {
		v := len(strings.Split(sentence, " "))
		if v > max {
			max = v
		}
	}
	return max
}
