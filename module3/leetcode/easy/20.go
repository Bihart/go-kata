package main

func kidsWithCandies(candies []int, extraCandies int) []bool { //nolint:all
	max := 0
	r := make([]bool, len(candies))

	for _, candy := range candies {
		if candy > max {
			max = candy
		}
	}
	for i := 0; i < len(candies); i++ {
		r[i] = candies[i]+extraCandies >= max
	}
	return r
}
