package main

func createTargetArray(nums []int, index []int) []int { //nolint:all
	r := make([]int, len(index))
	for i := 0; i < len(index); i++ {
		r[len(index)-1] = nums[i]
		for j := index[i]; j < len(index); j++ {
			r[len(index)-1], r[j] = r[j], r[len(index)-1]
		}
	}
	return r
}
