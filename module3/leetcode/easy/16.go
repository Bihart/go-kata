package main

func smallestEvenMultiple(n int) int { //nolint:all
	if n%2 == 0 {
		return n
	}
	return n * 2
}
