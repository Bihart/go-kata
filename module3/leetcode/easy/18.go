package main

func differenceOfSum(nums []int) int { //nolint:all
	n1, n2 := 0, 0

	for _, num := range nums {
		n1 += num

		for num/10 != 0 {
			n2 += num % 10
			num /= 10
		}
		n2 += num
	}
	return n1 - n2
}
