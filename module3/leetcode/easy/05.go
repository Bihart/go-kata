package main

func numberOfMatches(n int) int { //nolint:all
	ret := 0

	for ; n > 1; n /= 2 {
		ret += n / 2
		if n%2 > 0 {
			n++
		}
	}
	return ret
}
