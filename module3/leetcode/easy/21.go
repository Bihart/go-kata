package main

func subtractProductAndSum(n int) int { //nolint:all
	product := 1
	sum := 0
	for n != 0 {
		x := n % 10
		product *= x
		sum += x
		n /= 10
	}
	return product - sum
}
