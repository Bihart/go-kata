package main

//Вам дается неотрицательное дробное число с двумя знаками после запятой, указывающее температуру в градусах Цельсия.
//
//Вы должны преобразовать Цельсий в Кельвины и Фаренгейты и вернуть его в виде массива ans = [kelvin, fahrenheit].
//
//Верните массив ans. Ответы в пределах 10-5 от реального ответа будут приняты.

// Kelvin = Celsius + 273.15
//
// Fahrenheit = Celsius*1.80 + 32.00
func convertTemperature(celsius float64) []float64 { //nolint:all
	return []float64{celsius + 273.15, celsius*1.80 + 32.00}
}
