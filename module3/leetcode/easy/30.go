package main

func countGoodTriplets(arr []int, a int, b int, c int) int { //nolint:all
	count := 0
	for i := 0; i < len(arr)-2; i++ {
		for j := i + 1; j < len(arr)-1; j++ {
			aa := arr[i] - arr[j]
			if aa < 0 {
				aa = -aa
			}
			if aa <= a {
				for k := j + 1; k < len(arr); k++ {
					kk := arr[j] - arr[k]
					cc := arr[i] - arr[k]
					if kk < 0 {
						kk = -kk
					}
					if cc < 0 {
						cc = -cc
					}
					if kk <= b && cc <= c {
						count++
					}
				}
			}
		}
	}
	return count
}
