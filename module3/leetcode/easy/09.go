package main

func finalValueAfterOperations(operations []string) int { //nolint:all
	var x int
	for _, operation := range operations {
		if operation == "++X" || operation == "X++" {
			x++
		} else {
			x--
		}
	}
	return x
}
