package main

//Учитывая целочисленный массив nums длины n, вы хотите создать массив ans длины 2n, где ans[i] == nums[i] и ans[i + n] == nums[i] для 0 <= i < n (с индексом 0).
//
//В частности, ans это объединение двух nums массивов.
//
//Возвращает массив ans.

func getConcatenation(nums []int) []int { //nolint:all
	re := make([]int, len(nums)*2)

	for i := 0; i < len(nums); i++ {
		re[i] = nums[i]
		re[i+len(nums)] = nums[i]
	}

	return re
}
