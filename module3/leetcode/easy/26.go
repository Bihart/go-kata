package main

func decompressRLElist(nums []int) []int { //nolint:all
	length := 0
	for i := 0; i < len(nums); i += 2 {
		length += nums[i]
	}
	r := make([]int, length)

	for i, j := 0, 0; i < len(nums); i += 2 {
		for k := 0; k < nums[i]; k++ {
			r[j] = nums[i+1]
			j++
		}
	}
	return r
}
