package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode { //nolint:all
	node := head.Next
	returnNode := &ListNode{}
	tailReturnNode := returnNode
	for node.Next != nil {
		if node.Val == 0 {
			tailReturnNode.Next = &ListNode{}
			tailReturnNode = tailReturnNode.Next
		} else {
			tailReturnNode.Val += node.Val
		}
		node = node.Next
	}
	return returnNode
}
