package main

func numTilePossibilities(tiles string) int { //nolint:all
	wordSet := make(map[string]struct{})
	//wordSet11 := make(map[string]struct{})

	chars := make([]rune, len(tiles))
	for i, r := range tiles {
		chars[i] = r
	}
	for i := 0; i < len(chars); i++ {
		//fmt.Printf("		%s\n", string(chars))
		chars[0], chars[i] = chars[i], chars[0]
		dde([]rune{chars[0]}, chars[1:], &wordSet)

	}

	return len(wordSet)
}

func dde(c, s []rune, wordSet *map[string]struct{}) { //nolint:all
	(*wordSet)[string(c)] = struct{}{}
	//fmt.Printf("%s\n", string(c))
	if len(s) > 0 {
		for i := 0; i < len(s); i++ {
			s[0], s[i] = s[i], s[0]
			dde(append(c, s[0]), s[1:], wordSet)
			s[0], s[i] = s[i], s[0]
		}
	}
}
