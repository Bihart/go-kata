package main

func pairSum(head *ListNode) int { //nolint:all
	len := 1
	node := head
	for node.Next != nil {
		node = node.Next
		len++
	}
	node = head

	twinSums := make([]int, len/2)

	for i := 0; i < len/2; i++ {
		twinSums[i] = node.Val
		node = node.Next
	}
	for i := (len / 2) - 1; i >= 0; i-- {
		twinSums[i] += node.Val
		node = node.Next
	}
	var max int
	for i := 0; i < len/2; i++ {
		if twinSums[i] > max {
			max = twinSums[i]
		}
	}
	return max

}
