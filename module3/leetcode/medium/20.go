package main

func processQueries(queries []int, m int) []int { //nolint:all
	p := make([]int, m)
	for i := 0; i < m; i++ {
		p[i] = i + 1
	}

	for i := 0; i < len(queries); i++ {
		for j := 0; j < len(p); j++ {
			if queries[i] == p[j] {
				queries[i] = j
				for k := 0; k < j; k++ {
					p[k], p[j] = p[j], p[k]
				}
				break
			}
		}
	}
	return queries
}
