package main

func constructMaximumBinaryTree(nums []int) *TreeNode { //nolint:all
	return buildBranch(nums)
}
func buildBranch(nums []int) *TreeNode { //nolint:all
	if len(nums) == 1 {
		return &TreeNode{Val: nums[0]}
	}
	if len(nums) <= 0 {
		return nil
	}
	var poz int
	var max int
	for i, num := range nums {
		if num > max {
			max = num
			poz = i
		}
	}

	return &TreeNode{
		Val:   max,
		Left:  buildBranch(nums[:poz]),
		Right: buildBranch(nums[poz+1:]),
	}
}
