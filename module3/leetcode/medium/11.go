package main

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func removeLeafNodes(root *TreeNode, target int) *TreeNode { //nolint:all
	if dellT(root, target) {
		return nil
	}
	return root
}

func dellT(root *TreeNode, target int) bool { //nolint:all
	if root.Right != nil {
		if dellT(root.Right, target) {
			root.Right = nil
		}
	}
	if root.Left != nil {

		if dellT(root.Left, target) {
			root.Left = nil
		}
	}
	if root.Val == target && root.Left == nil && root.Right == nil {
		return true
	}
	return false

}
