package main

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode { //nolint:all
	nodeA := list1

	for i := 0; i < a-1; i++ {
		nodeA = nodeA.Next
	}

	nodeB := nodeA
	for i := 0; i < b-a; i++ {
		nodeB = nodeB.Next
	}

	nodeA.Next = list2

	for nodeA.Next != nil {
		nodeA = nodeA.Next
	}
	nodeA.Next = nodeB

	return list1
}
