package main

import (
	"sort"
)

func balanceBST(root *TreeNode) *TreeNode { //nolint:all
	var mas []int
	getAllVal(root, &mas)
	sort.Ints(mas)
	return binarySearchTree(mas)
}

func getAllVal(branch *TreeNode, mas *[]int) { //nolint:all
	*mas = append(*mas, branch.Val)
	if branch.Right != nil {
		getAllVal(branch.Right, mas)
	}
	if branch.Left != nil {
		getAllVal(branch.Left, mas)
	}
}
func binarySearchTree(mas []int) *TreeNode { //nolint:all
	if len(mas) == 1 {
		return &TreeNode{Val: mas[0]}
	}
	if len(mas) <= 0 {
		return nil
	}

	return &TreeNode{
		Val:   mas[(len(mas) / 2)],
		Left:  binarySearchTree(mas[:(len(mas) / 2)]),
		Right: binarySearchTree(mas[(len(mas)/2)+1:]),
	}
}
