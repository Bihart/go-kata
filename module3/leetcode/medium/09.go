package main

import (
	"sort"
)

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool { //nolint:all
	ret := make([]bool, len(l))

	for i := 0; i < len(l); i++ {
		source := nums[l[i] : r[i]+1]
		dest := make([]int, len(source))
		_ = copy(dest, source)
		sort.Ints(dest)

		m := dest[0] - dest[1]
		fl := true
		for ii := 0; ii < len(dest)-1; ii++ {
			if dest[ii]-dest[ii+1] != m {
				fl = false
				break
			}
		}
		ret[i] = fl
	}

	return ret
}
