package main

func minPartitions(n string) int { //nolint:all
	max := 0
	for _, s := range n {
		iS := int(s)
		if iS > max {
			max = iS
		}
	}
	return max - 48
}
