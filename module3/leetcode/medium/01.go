package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deepestLeavesSum(root *TreeNode) int { //nolint:all
	var nodes = []*TreeNode{root}
	noEnd := true
	for noEnd {
		newNodes := make([]*TreeNode, 0, len(nodes))
		for i := 0; i < len(nodes); i++ {
			if nodes[i].Left != nil {
				newNodes = append(newNodes, nodes[i].Left)
			}
			if nodes[i].Right != nil {
				newNodes = append(newNodes, nodes[i].Right)
			}
		}

		if len(newNodes) <= 0 {
			break
		}
		nodes = newNodes
	}

	var n int
	for _, node := range nodes {
		n += node.Val
	}
	return n

}
