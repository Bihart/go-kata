package main

/*type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}*/

func bstToGst(root *TreeNode) *TreeNode { //nolint:all
	upBranch(root, 0)

	return root

}
func upBranch(root *TreeNode, n int) int { //nolint:all
	if root.Right != nil {
		n = upBranch(root.Right, n)
	}
	root.Val += n
	n = root.Val

	if root.Left != nil {
		n = upBranch(root.Left, n)
	}

	return n
}
