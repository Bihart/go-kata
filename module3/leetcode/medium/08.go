package main

func findSmallestSetOfVertices(n int, edges [][]int) []int { //nolint:all
	vertices := make([]int, n)
	for _, edge := range edges {
		vertices[edge[1]]++
	}

	ret := make([]int, 0)
	for i, ingoing := range vertices {
		if ingoing == 0 {
			ret = append(ret, i)
		}
	}

	return ret
}
