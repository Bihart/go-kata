package main

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

func averageOfSubtree(root *TreeNode) int { //nolint:all
	var count int
	average(root, &count)
	return count
}

func average(root *TreeNode, count *int) (mas []int) { //nolint:all
	if root.Right != nil {
		m := average(root.Right, count)
		mas = append(mas, m...)
	}
	if root.Left != nil {
		m := average(root.Left, count)
		mas = append(mas, m...)
	}

	mas = append(mas, root.Val)

	x := 0
	for _, ma := range mas {
		x += ma
	}
	x /= len(mas)

	if x == root.Val {
		*count++
	}

	return mas
}
