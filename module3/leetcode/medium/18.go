package main

func groupThePeople(groupSizes []int) [][]int { //nolint:all
	var temp = make([][]int, 500)
	var r [][]int
	for i, size := range groupSizes {
		temp[size] = append(temp[size], i)
		if len(temp[size]) == size {
			r = append(r, temp[size])
			temp[size] = make([]int, 0, size)
		}
	}
	return r
}
