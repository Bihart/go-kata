package main

func countPoints(points [][]int, queries [][]int) []int { //nolint:all
	var r = make([]int, len(queries))
	for i, query := range queries {
		for _, point := range points {
			xa := query[0] - point[0]
			tb := query[1] - point[1]
			if (xa*xa)+(tb*tb) <= query[2]*query[2] {
				r[i]++
			}
		}
	}
	return r
}
