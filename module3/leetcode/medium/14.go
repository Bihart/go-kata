package main

func xorQueries(arr []int, queries [][]int) []int { //nolint:all
	r := make([]int, len(queries))
	for i := 0; i < len(r); i++ {
		for j := queries[i][0]; j <= queries[i][1]; j++ {
			r[i] = r[i] ^ arr[j]
		}
	}
	return r
}
