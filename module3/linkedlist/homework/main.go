package main

import (
	"errors"
	"fmt"
	"math/rand"

	"gopkg.in/loremipsum.v1"
)

// Post Создать структуру Post с полями:
//
// body (тип string) - текст поста.
// publishDate (тип int64) - дата публикации поста в формате Unix timestamp.
// next (тип *Post) - указатель на следующий пост в потоке.
type Post struct {
	body        string
	publishDate int64
	next        *Post
}

// Feed Создать структуру Feed с полями:
//
// length (тип int) - количество постов в потоке.
// start (тип *Post) - указатель на первый пост в потоке.
// end (тип *Post) - указатель на последний пост в потоке.
type Feed struct {
	length int
	start  *Post
	end    *Post
}

// Append Реализовать метод Append для структуры Feed:
//
// Метод принимает указатель на новый пост (newPost *Post).
// Метод добавляет новый пост в конец потока.
func (feed *Feed) Append(newPost *Post) {
	if feed.length == 0 {
		feed.start = newPost
		feed.end = newPost
	} else {
		newPost.publishDate = feed.end.publishDate + 1
		feed.end.next = newPost
		feed.end = newPost
	}
	feed.length++
}

// Remove Реализовать метод Remove для структуры Feed:
//
// Метод принимает дату публикации (publishDate int64).
// Метод удаляет пост с заданной датой публикации из потока.
func (feed *Feed) Remove(publishDate int64) {
	if feed.length <= 0 {
		panic(errors.New("feed is empty"))
	}

	var prevPost *Post
	post := feed.start

	for post.publishDate != publishDate {
		if post.next == nil {
			panic(errors.New("missing Post with specified publishDate"))
		}

		prevPost = post
		post = prevPost.next
	}

	if feed.end == post {
		feed.end = prevPost
	}
	prevPost.next = post.next

	feed.length--
}

// Insert Реализовать метод Insert для структуры Feed:
//
// Метод принимает указатель на новый пост (newPost *Post).
// Метод вставляет новый пост в поток с учетом его даты публикации (посты должны быть отсортированы по дате публикации).
func (feed *Feed) Insert(newPost *Post) {
	if feed.length == 0 {
		feed.start = newPost
		feed.end = newPost
		return
	} else if feed.end.publishDate < newPost.publishDate {
		feed.end.next = newPost
		feed.end = newPost
	} else if feed.start.publishDate >= newPost.publishDate {
		newPost.next = feed.start
		feed.start = newPost
	} else {
		var prevPost *Post
		post := feed.start

		for post.publishDate < newPost.publishDate {
			prevPost = post
			post = prevPost.next
		}

		newPost.next = post
		prevPost.next = newPost
	}
	feed.length++
}

// Inspect Реализовать метод Inspect для структуры Feed:
//
// Метод выводит информацию о потоке и его постах.
func (feed *Feed) Inspect() {
	fmt.Printf("number of posts: %d\n", feed.length)
	post := feed.start
	for i := 0; i < feed.length; i++ {
		fmt.Printf("Post %d\n", i)
		fmt.Printf("	Body %s\n", post.body)
		fmt.Printf("	Date: %d\n", post.publishDate)
		fmt.Printf("	Next post: %v\n", post.next)
		post = post.next
	}
}

// Написать функцию main, которая создает несколько постов, добавляет их в поток, а затем выводит информацию о потоке с помощью метода Inspect.
func main() {
	loremIpsumGeneratoe := loremipsum.New()
	var feed Feed

	for i := 0; i < rand.Intn(25); i++ {
		feed.Append(&Post{body: loremIpsumGeneratoe.Words(rand.Intn(10)), publishDate: rand.Int63()})
	}

	feed.Inspect()

	for i := 0; i < rand.Intn(25); i++ {
		feed.Insert(&Post{body: loremIpsumGeneratoe.Words(rand.Intn(10)), publishDate: rand.Int63()})
	}

	feed.Inspect()
}
