package test

import (
	"fmt"
	"os"
	"reflect"
	"testing"

	. "gitlab.com/gerzhod/go-kata/module3/clean_architecture/service/repo"

	"gitlab.com/gerzhod/go-kata/module3/clean_architecture/service/model"
)

func TestFileTodoRepository_CreateTodo(t *testing.T) {
	type fields struct {
		FilePath string
		f        func()
	}
	type args struct {
		task model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "empty File",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_ = create.Close()
					},
				}
			}(),
			args: args{task: model.Todo{ID: 123, Title: "Lorem"}},
			want: []byte{
				123, 34, 105, 100, 34, 58, 49, 50, 51, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101,
				109, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116,
				97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 00, 10,
			},
			wantErr: false,
		},
		{
			name: "non empty File",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_, _ = create.Write([]byte{
							123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109,
							49, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115,
							116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 50,
							44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 50, 34, 44, 34, 100, 101, 115,
							99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58,
							102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116, 105, 116, 108, 101,
							34, 58, 34, 76, 111, 114, 101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111,
							110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0,
							10})
						_ = create.Close()
					},
				}
			}(),
			args: args{task: model.Todo{ID: 123, Title: "Lorem"}},
			want: []byte{
				123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 49,
				34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97,
				116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 50, 44, 34, 116,
				105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 50, 34, 44, 34, 100, 101, 115, 99, 114, 105,
				112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115,
				101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111,
				114, 101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44,
				34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58,
				49, 50, 51, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 34, 44, 34, 100, 101,
				115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58,
				102, 97, 108, 115, 101, 125, 0, 10,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := FileTodoRepository{
				FilePath: tt.fields.FilePath,
			}
			tt.fields.f()
			_, err := repo.CreateTodo(tt.args.task)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				file, _ := os.ReadFile(tt.fields.FilePath)
				if len(tt.want) == len(file) {
					for i := 0; i < len(file); i++ {
						if file[i] != tt.want[i] {
							t.Errorf("CreateTodo() file = %v, want %v", file, tt.want)
							break
						}
					}
				} else {
					t.Errorf("CreateTodo() file = %v, want %v", file, tt.want)
				}

			}
		})
	}
}

func TestFileTodoRepository_DeleteTodo(t *testing.T) {
	type fields struct {
		FilePath string
		f        func()
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "empty File",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_ = create.Close()
					},
				}
			}(),
			args:    args{id: 123},
			wantErr: true,
		},
		{
			name: "non empty File/no have id",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_, _ = create.Write([]byte{
							123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109,
							49, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115,
							116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 50,
							44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 50, 34, 44, 34, 100, 101, 115,
							99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58,
							102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116, 105, 116, 108, 101,
							34, 58, 34, 76, 111, 114, 101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111,
							110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0,
							10})
						_ = create.Close()
					},
				}
			}(),
			args: args{id: 123},
			want: []byte{
				123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 49,
				34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97,
				116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 50, 44, 34, 116,
				105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 50, 34, 44, 34, 100, 101, 115, 99, 114, 105,
				112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115,
				101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111,
				114, 101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44,
				34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10,
			},
			wantErr: true,
		},
		{
			name: "non empty File/have id",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_, _ = create.Write([]byte{
							123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109,
							49, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115,
							116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 50,
							44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 50, 34, 44, 34, 100, 101, 115,
							99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58,
							102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116, 105, 116, 108, 101,
							34, 58, 34, 76, 111, 114, 101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111,
							110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0,
							10})
						_ = create.Close()
					},
				}
			}(),
			args: args{id: 2},
			want: []byte{
				123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 49,
				34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97,
				116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116,
				105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112,
				116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101,
				125, 0, 10,
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := FileTodoRepository{
				FilePath: tt.fields.FilePath,
			}
			tt.fields.f()
			if err := repo.DeleteTodo(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("DeleteTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
			file, _ := os.ReadFile(tt.fields.FilePath)
			if len(tt.want) == len(file) {
				for i := 0; i < len(file); i++ {
					if file[i] != tt.want[i] {
						t.Errorf("DeleteTodo() file = %v, want %v", file, tt.want)
						break
					}
				}
			} else {
				t.Errorf("DeleteTodo() file = %v\n, want %v", file, tt.want)
			}
		})
	}
}

func TestFileTodoRepository_GetTodo(t *testing.T) {
	type fields struct {
		FilePath string
		f        func()
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Todo
		wantErr bool
	}{
		{
			name: "empty File",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_ = create.Close()
					},
				}
			}(),
			args:    args{id: 123},
			wantErr: true,
		},
		{
			name: "non empty File/no have id",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_, _ = create.Write([]byte{
							123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109,
							49, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115,
							116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 50,
							44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 50, 34, 44, 34, 100, 101, 115,
							99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58,
							102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116, 105, 116, 108, 101,
							34, 58, 34, 76, 111, 114, 101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111,
							110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0,
							10})
						_ = create.Close()
					},
				}
			}(),
			args:    args{id: 123},
			wantErr: true,
		},
		{
			name: "non empty File/have id",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_, _ = create.Write([]byte{
							123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109,
							49, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115,
							116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 50,
							44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 50, 34, 44, 34, 100, 101, 115,
							99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58,
							102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116, 105, 116, 108, 101,
							34, 58, 34, 76, 111, 114, 101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111,
							110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0,
							10})
						_ = create.Close()
					},
				}
			}(),
			args:    args{id: 2},
			want:    model.Todo{ID: 2, Title: "Lorem2"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := FileTodoRepository{
				FilePath: tt.fields.FilePath,
			}
			tt.fields.f()
			got, err := repo.GetTodo(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTodo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTodo() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTodoRepository_GetTodos(t *testing.T) {
	type fields struct {
		FilePath string
		f        func()
	}
	tests := []struct {
		name    string
		fields  fields
		want    []model.Todo
		wantErr bool
	}{
		{
			name: "empty File",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_ = create.Close()
					},
				}
			}(),
			want:    nil,
			wantErr: false,
		},
		{
			name: "non empty File",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_, _ = create.Write([]byte{
							123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109,
							49, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115,
							116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 50,
							44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 50, 34, 44, 34, 100, 101, 115,
							99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58,
							102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116, 105, 116, 108, 101,
							34, 58, 34, 76, 111, 114, 101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111,
							110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0,
							10})
						_ = create.Close()
					},
				}
			}(),
			want:    []model.Todo{{ID: 1, Title: "Lorem1"}, {ID: 2, Title: "Lorem2"}, {ID: 3, Title: "Lorem3"}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := FileTodoRepository{
				FilePath: tt.fields.FilePath,
			}
			tt.fields.f()
			got, err := repo.GetTodos()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTodos() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTodos() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTodoRepository_UpdateTodo(t *testing.T) {
	type fields struct {
		FilePath string
		f        func()
	}
	type args struct {
		task model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "empty File",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_ = create.Close()
					},
				}
			}(),
			args:    args{model.Todo{ID: 123}},
			wantErr: true,
		},
		{
			name: "non empty File/no have id",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_, _ = create.Write([]byte{
							123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109,
							49, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115,
							116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 50,
							44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 50, 34, 44, 34, 100, 101, 115,
							99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58,
							102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116, 105, 116, 108, 101,
							34, 58, 34, 76, 111, 114, 101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111,
							110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0,
							10})
						_ = create.Close()
					},
				}
			}(),
			args: args{model.Todo{ID: 123}},
			want: []byte{
				123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 49,
				34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97,
				116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 50, 44, 34, 116,
				105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 50, 34, 44, 34, 100, 101, 115, 99, 114, 105,
				112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115,
				101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111,
				114, 101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44,
				34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10,
			},
			wantErr: true,
		},
		{
			name: "non empty File/have id",
			fields: func() fields {
				s := fmt.Sprintf("./%s.json", t.Name())
				return fields{FilePath: s,
					f: func() {
						create, _ := os.Create(s)
						_, _ = create.Write([]byte{
							123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109,
							49, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115,
							116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 50,
							44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 50, 34, 44, 34, 100, 101, 115,
							99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58,
							102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116, 105, 116, 108, 101,
							34, 58, 34, 76, 111, 114, 101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111,
							110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0,
							10})
						_ = create.Close()
					},
				}
			}(),
			args: args{model.Todo{ID: 2, Title: "Lorem"}},
			want: []byte{
				123, 34, 105, 100, 34, 58, 49, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 49,
				34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97,
				116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10, 123, 34, 105, 100, 34, 58, 50, 44, 34, 116,
				105, 116, 108, 101, 34, 58, 34, 76, 111, 114, 101, 109, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112,
				116, 105, 111, 110, 34, 58, 34, 34, 44, 34, 115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101,
				125, 0, 10, 123, 34, 105, 100, 34, 58, 51, 44, 34, 116, 105, 116, 108, 101, 34, 58, 34, 76, 111, 114,
				101, 109, 51, 34, 44, 34, 100, 101, 115, 99, 114, 105, 112, 116, 105, 111, 110, 34, 58, 34, 34, 44, 34,
				115, 116, 97, 116, 117, 115, 34, 58, 102, 97, 108, 115, 101, 125, 0, 10,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := FileTodoRepository{
				FilePath: tt.fields.FilePath,
			}
			tt.fields.f()
			_, err := repo.UpdateTodo(tt.args.task)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateTodo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			file, _ := os.ReadFile(tt.fields.FilePath)
			if len(tt.want) == len(file) {
				for i := 0; i < len(file); i++ {
					if file[i] != tt.want[i] {
						t.Errorf("DeleteTodo() file = %v, want %v", file, tt.want)
						break
					}
				}
			} else {
				t.Errorf("DeleteTodo() file = %v\n, want %v", file, tt.want)
			}
		})
	}
}
