package repo

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"

	. "gitlab.com/gerzhod/go-kata/module3/clean_architecture/service/model"
)

// Repository layer

type TodoRepository interface {
	GetTodos() ([]Todo, error)
	GetTodo(id int) (Todo, error)
	CreateTodo(task Todo) (Todo, error)
	UpdateTodo(task Todo) (Todo, error)
	DeleteTodo(id int) error
}

type FileTodoRepository struct {
	FilePath string
	wg       sync.WaitGroup
}

func (repo *FileTodoRepository) GetTodos() ([]Todo, error) {
	var todos []Todo
	length := 1024
	var err error
	file, err := os.OpenFile(repo.FilePath, os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		return nil, err
	}

	chanBytes := make(chan []byte, 2)
	go func(n int) {
		repo.wg.Wait()
		repo.wg.Add(1)
		defer func() {
			err = file.Close()
			repo.wg.Done()
			close(chanBytes)
		}()
		for n >= length {
			bytes := make([]byte, length)
			n, err = file.Read(bytes)
			chanBytes <- bytes[:n]
		}
	}(length)
	var bytes []byte
	var todo Todo
	if err != nil {
		return todos, err
	}
	i := 0
	for receivedBytes := range chanBytes {
		bytes = append(bytes, receivedBytes...)

		for ; i < len(bytes); i++ {
			if bytes[i] == 0 {
				errJson := json.Unmarshal(bytes[:i], &todo)
				if errJson == nil {
					todos = append(todos, todo)
				}
				bytes = bytes[i+2:]
				i = 0
				continue
			}
		}

	}
	return todos, err
}

func (repo *FileTodoRepository) GetTodo(id int) (Todo, error) {
	length := 1024
	var err error
	file, err := os.Open(repo.FilePath)
	if err != nil {
		return Todo{}, err
	}

	found := false
	chanBytes := make(chan []byte)

	go func(n int) {
		repo.wg.Wait()
		repo.wg.Add(1)
		defer func() {
			err = file.Close()
			repo.wg.Done()
			close(chanBytes)
		}()
		for n >= length {
			if found {
				break
			}
			bytes := make([]byte, length)
			n, err = file.Read(bytes)
			chanBytes <- bytes[:n]
		}
	}(length)
	var bytes []byte
	var todo Todo
	if err != nil {
		return todo, err
	}
	i := 0
	for receivedBytes := range chanBytes {
		bytes = append(bytes, receivedBytes...)

		for ; i < len(bytes); i++ {
			if bytes[i] == 0 {
				errJson := json.Unmarshal(bytes[:i], &todo)
				if errJson == nil && todo.ID == id {
					found = true
					return todo, nil
				}
				bytes = bytes[i+2:]
				i = 0
				continue
			}
		}

	}
	return Todo{}, fmt.Errorf("todo not found")
}

func (repo *FileTodoRepository) CreateTodo(task Todo) (Todo, error) {
	//file, err := os.OpenFile(repo.FilePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC|os.O_APPEND, 0666)
	file, err := os.OpenFile(repo.FilePath, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return Todo{}, err
	}

	bytes, err := json.Marshal(task)
	if err != nil {
		return Todo{}, err
	}

	bytes = append(bytes, 00, 10)

	_, err = file.Write(bytes)
	if err != nil {
		return task, err
	}

	return task, nil
}

func (repo *FileTodoRepository) UpdateTodo(task Todo) (Todo, error) {
	todos, err := repo.GetTodos()
	if err != nil {
		return Todo{}, err
	}

	repo.wg.Wait()
	repo.wg.Add(1)
	defer repo.wg.Done()
	file, err := os.OpenFile(repo.FilePath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		return Todo{}, err
	}
	defer func(file *os.File) {
		_ = file.Close()
	}(file)

	fiend := false
	for i := 0; i < len(todos); i++ {
		if todos[i].ID == task.ID {
			fiend = true
			err = write(task, file)
			if err != nil {
				continue
			}
		} else {
			err = write(todos[i], file)
			if err != nil {
				continue
			}
		}

	}
	if fiend {
		return task, err
	}
	return Todo{}, fmt.Errorf("not found %w", err)

}

func (repo *FileTodoRepository) DeleteTodo(id int) error {
	todos, err := repo.GetTodos()
	if err != nil {
		return err
	}
	fiend := false

	repo.wg.Wait()
	repo.wg.Add(1)
	defer repo.wg.Done()
	file, err := os.OpenFile(repo.FilePath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}
	defer func(file *os.File) {
		_ = file.Close()
	}(file)

	for i := 0; i < len(todos); i++ {
		if todos[i].ID != id {
			err = write(todos[i], file)
			if err != nil {
				continue
			}
		} else {
			fiend = true
		}

	}
	if fiend {
		return err
	}
	return fmt.Errorf("not found %w", err)
}

func write(task Todo, file *os.File) error {
	marshal, err := json.Marshal(task)
	if err != nil {
		return err
	}
	marshal = append(marshal, 00, 10)
	_, err = file.Write(marshal)
	if err != nil {
		return err
	}
	return nil
}
