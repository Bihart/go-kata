package service

import (
	"math/rand"

	. "gitlab.com/gerzhod/go-kata/module3/clean_architecture/service/model"
	. "gitlab.com/gerzhod/go-kata/module3/clean_architecture/service/repo"
)

type TodoService interface {
	ListTodos() ([]Todo, error)
	CreateTodo(title string) error
	CompleteTodo(todo Todo) error
	RemoveTodo(todo Todo) error
}

type todoService struct {
	repository TodoRepository
}

func GetService(repository TodoRepository) TodoService {
	return &todoService{repository: repository}
}

func (s *todoService) ListTodos() ([]Todo, error) {
	return s.repository.GetTodos()
}

func (s *todoService) CreateTodo(title string) error {
	_, err := s.repository.CreateTodo(
		Todo{
			Title: title,
			ID:    rand.Int(),
		})
	return err
}

func (s *todoService) CompleteTodo(todo Todo) error {
	todo.IsComplete = true
	_, err := s.repository.UpdateTodo(todo)
	if err != nil {
		return err
	}
	return err
}

func (s *todoService) RemoveTodo(todo Todo) error {
	return s.repository.DeleteTodo(todo.ID)
}
