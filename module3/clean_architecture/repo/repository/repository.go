package main

import (
	"encoding/json"
	"fmt"
	"io"
	"sync"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	wg   sync.WaitGroup
	File io.ReadWriter
}

func NewUserRepository(file io.ReadWriter) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record interface{}) error {
	switch record.(type) {
	case User:
		break
	default:
		return fmt.Errorf("unknown record type")
	}

	bytes, err := json.Marshal(record)
	if err != nil {
		return err
	}
	if err != nil {
		return err
	}
	r.wg.Wait()
	r.wg.Add(2)
	_, err = r.File.Write(bytes)
	r.wg.Done()
	if err != nil {
		return err
	}
	_, err = r.File.Write([]byte{00, 10})
	r.wg.Done()
	if err != nil {
		return err
	}
	return nil

}

func (r *UserRepository) Find(id int) (interface{}, error) {
	length := 512
	var err error
	var bytes []byte

	chanBytes := make(chan []byte)
	defer func() {
		_, ok := <-chanBytes
		if ok {
			close(chanBytes)
		}
	}()

	go func(x int) {
		r.wg.Wait()
		r.wg.Add(1)
		defer r.wg.Done()
		defer close(chanBytes)
		n := x

		for n >= x {
			var bytesOne = make([]byte, x)
			n, err = r.File.Read(bytesOne)
			if err != nil {
				break
			}
			chanBytes <- bytesOne[:n]
		}

	}(length)

	i := 0
	if err != nil {
		return nil, err
	}
	for chanByte := range chanBytes {

		bytes = append(bytes, chanByte...)
		cont := true

		for ; i < len(bytes); i++ {
			if bytes[i] == 0 {
				cont = false

				var user User
				err = json.Unmarshal(bytes[:i], &user)
				if err != nil {
					return nil, err
				}
				if user.ID == id {
					return user, nil
				}
				bytes = bytes[i+2:]
				i = 0
			}
		}
		if cont {
			continue
		}

		if err != nil {
			return nil, err
		}
	}
	return nil, nil
}

func (r *UserRepository) FindAll() ([]User, error) {
	length := 512
	var err error
	var bytes []byte

	chanBytes := make(chan []byte)
	defer func() {
		_, ok := <-chanBytes
		if ok {
			close(chanBytes)
		}
	}()
	go func(x int) {
		r.wg.Wait()
		r.wg.Add(1)
		defer r.wg.Done()

		defer close(chanBytes)
		n := x

		for n >= x {
			var bytesOne = make([]byte, x)
			n, err = r.File.Read(bytesOne)
			if err != nil {
				break
			}
			chanBytes <- bytesOne[:n]
		}
	}(length)

	i := 0
	var users []User

	if err != nil {
		return users, err
	}
	for chanByte := range chanBytes {

		bytes = append(bytes, chanByte...)
		cont := true

		for ; i < len(bytes); i++ {
			if bytes[i] == 0 {
				cont = false

				var user User
				err = json.Unmarshal(bytes[:i], &user)
				if err != nil {
					return users, err
				}

				users = append(users, user)
				bytes = bytes[i+2:]
				i = 0
			}
		}
		if cont {
			continue
		}

		if err != nil {
			return users, err
		}
	}
	if err != nil {
		return users, err
	}
	return users, nil
}
