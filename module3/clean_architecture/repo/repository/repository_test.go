package main

import (
	"fmt"
	"io"
	"io/fs"
	"os"
	"reflect"
	"testing"
)

func TestUserRepository_Find(t *testing.T) {
	type fields struct {
		File io.ReadWriter
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			fields: func() fields {
				f, err := os.OpenFile("./test_find.json", os.O_WRONLY|os.O_SYNC|os.O_CREATE, fs.ModePerm)
				if err != nil {
					panic(err)
				}
				_, err = f.Write([]byte{
					123, 34, 105, 100, 34, 58, 49, 44, 34, 110, 97, 109, 101, 34, 58, 34, 49, 34, 125, 0, 10, 123, 34,
					105, 100, 34, 58, 50, 44, 34, 110, 97, 109, 101, 34, 58, 34, 50, 34, 125, 0, 10, 123, 34, 105, 100,
					34, 58, 51, 44, 34, 110, 97, 109, 101, 34, 58, 34, 51, 34, 125, 0, 10,
				})
				if err != nil {
					panic(err)
				}
				err = f.Close()
				if err != nil {
					return fields{}
				}
				f, err = os.OpenFile("./test_find.json", os.O_RDWR|os.O_SYNC|os.O_CREATE, fs.ModePerm)
				if err != nil {
					return fields{}
				}
				return fields{File: f}
			}(),
			args: args{id: 2},
			want: User{ID: 2, Name: "2"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			got, err := r.Find(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Find() got = %v, want %v", got, tt.want)
			}
		})

	}
}

func TestUserRepository_Save(t *testing.T) {
	type fields struct {
		File io.ReadWriter
	}
	type args struct {
		record interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			fields: func() fields {
				_, _ = os.Create("./test_save.json")
				f, err := os.OpenFile("./test_save.json", os.O_RDWR|os.O_SYNC|os.O_CREATE|os.O_APPEND, 0)
				if err != nil {
					panic(err)
				}
				fmt.Println(f.Name())
				return fields{File: f}
			}(),
			want: []byte{
				123, 34, 105, 100, 34, 58, 53, 52, 54, 56, 52, 49, 51, 53, 49, 51, 51, 56, 56, 44, 34, 110, 97, 109,
				101, 34, 58, 34, 116, 101, 115, 116, 85, 115, 101, 114, 34, 125, 0, 10, 123, 34, 105, 100, 34, 58, 53,
				52, 54, 56, 52, 49, 51, 53, 49, 51, 51, 56, 56, 44, 34, 110, 97, 109, 101, 34, 58, 34, 116, 101, 115,
				116, 85, 115, 101, 114, 34, 125, 0, 10,
			},
			args: args{record: User{Name: "testUser", ID: 5468413513388}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			_ = r.Save(tt.args.record)
			if err := r.Save(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			} else {
				file, _ := os.ReadFile("./test_save.json")
				if len(file) == len(tt.want) {
					for i := 0; i < len(file); i++ {
						if file[i] != tt.want[i] {
							t.Errorf("Save() file = %v, want %v", file, tt.want)
							break
						}
					}
				} else {
					t.Errorf("Save() file = %v, want %v", file, tt.want)
				}
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	type fields struct {
		File io.ReadWriter
	}
	var tests = []struct {
		name    string
		fields  fields
		want    []interface{}
		wantErr bool
	}{
		{

			fields: func() fields {
				f, err := os.OpenFile("./test_findall.json", os.O_WRONLY|os.O_SYNC|os.O_CREATE, fs.ModePerm)
				if err != nil {
					panic(err)
				}
				_, err = f.Write([]byte{
					123, 34, 105, 100, 34, 58, 49, 44, 34, 110, 97, 109, 101, 34, 58, 34, 49, 34, 125, 0, 10, 123, 34,
					105, 100, 34, 58, 50, 44, 34, 110, 97, 109, 101, 34, 58, 34, 50, 34, 125, 0, 10, 123, 34, 105, 100,
					34, 58, 51, 44, 34, 110, 97, 109, 101, 34, 58, 34, 51, 34, 125, 0, 10,
				})
				if err != nil {
					panic(err)
				}
				err = f.Close()
				if err != nil {
					return fields{}
				}
				f, err = os.OpenFile("./test_findall.json", os.O_RDWR|os.O_SYNC|os.O_CREATE, fs.ModePerm)
				if err != nil {
					return fields{}
				}
				return fields{File: f}
			}(),
			want: []interface{}{User{ID: 1, Name: "1"}, User{ID: 2, Name: "2"}, User{ID: 3, Name: "3"}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			got, err := r.FindAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("FindAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindAll() got = %v, want %v", got, tt.want)
			}
		})
	}
}
