package main

import "gitlab.com/gerzhod/go-kata/module3/clean_architecture/cli/cli"

func main() {
	cli.New().Run()
}
