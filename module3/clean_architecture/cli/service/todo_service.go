package service

import (
	"strings"

	. "gitlab.com/gerzhod/go-kata/module3/clean_architecture/service/model"
	. "gitlab.com/gerzhod/go-kata/module3/clean_architecture/service/repo"
)

type TodoService interface {
	ListTodos() ([]Todo, error)
	CreateTodo(title string, description string) error
	CompleteTodo(id int) error
	RemoveTodo(id int) error
	UpdateTodo(id int, title string, description string) error
}

type todoService struct {
	repository TodoRepository
	lastID     int
}

func GetService(repository TodoRepository) TodoService {
	var lastID int
	todos, err := repository.GetTodos()
	if err != nil {
		panic(err)
	}
	for _, todo := range todos {
		if todo.ID > lastID {
			lastID = todo.ID
		}
	}
	return &todoService{repository: repository}
}

func (s *todoService) ListTodos() ([]Todo, error) {
	return s.repository.GetTodos()
}

func (s *todoService) CreateTodo(title string, description string) error {
	s.lastID++
	_, err := s.repository.CreateTodo(
		Todo{
			Title:       strings.ReplaceAll(title, "\n", " "),
			Description: strings.ReplaceAll(description, "\n", " "),
			ID:          s.lastID,
		})
	return err
}

func (s *todoService) CompleteTodo(id int) error {
	todo, err := s.repository.GetTodo(id)
	if err != nil {
		return err
	}
	todo.IsComplete = true
	_, err = s.repository.UpdateTodo(todo)
	return err
}

func (s *todoService) RemoveTodo(id int) error {
	return s.repository.DeleteTodo(id)
}

func (s *todoService) UpdateTodo(id int, title string, description string) error {
	todo, err := s.repository.GetTodo(id)
	if err != nil {
		return err
	}
	if len(title) > 0 {
		todo.Title = title
	}
	if len(description) > 0 {
		todo.Description = description
	}

	_, err = s.repository.UpdateTodo(todo)
	if err != nil {
		return err
	}

	return nil
}
