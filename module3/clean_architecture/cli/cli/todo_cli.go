package cli

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	service "gitlab.com/gerzhod/go-kata/module3/clean_architecture/cli/service"
	"gitlab.com/gerzhod/go-kata/module3/clean_architecture/service/repo"

	"github.com/c-bata/go-prompt"
)

var (
	serv = service.GetService(&repo.FileTodoRepository{FilePath: "./DB.json"})
)
var (
	Show     = "show"
	Add      = "add"
	Exit     = "exit"
	Delete   = "delete"
	Edit     = "edit"
	Complete = "complete"
)

func New() *prompt.Prompt {
	return prompt.New(executor, completer)
}

func completer(d prompt.Document) []prompt.Suggest {
	var s []prompt.Suggest
	texts := strings.Split(strings.ToLower(d.Text), " ")

	switch texts[0] {
	case Delete:
		if len(texts) > 2 {
			break
		}

		todos, err := serv.ListTodos()
		if err != nil {
			s = []prompt.Suggest{
				{Text: "", Description: err.Error()},
			}
			break
		}

		for _, todo := range todos {
			s = append(s, prompt.Suggest{Text: fmt.Sprintf("%d", todo.ID), Description: todo.Title})
		}

	case Edit:
		if len(texts) > 3 {
			break
		}
		if len(texts) > 2 {
			todos, err := serv.ListTodos()
			if err != nil {
				s = []prompt.Suggest{
					{Text: "", Description: err.Error()},
				}
				break
			}

			for _, todo := range todos {
				s = append(s, prompt.Suggest{Text: fmt.Sprintf("%d", todo.ID), Description: todo.Title})
			}
			break
		}

		s = []prompt.Suggest{
			{Text: "title", Description: "edit Title id todo"},
			{Text: "t", Description: "edit Title id todo"},
			{Text: "description", Description: "edit Description id todo"},
			{Text: "d", Description: "edit Description id todo"},
		}
	case Complete:
		if len(texts) > 2 {
			break
		}

		todos, err := serv.ListTodos()
		if err != nil {
			s = []prompt.Suggest{
				{Text: "", Description: err.Error()},
			}
			break
		}

		for _, todo := range todos {
			s = append(s, prompt.Suggest{Text: fmt.Sprintf("%d", todo.ID), Description: todo.Title})
		}

	default:
		s = []prompt.Suggest{
			{Text: "Show", Description: "Show a list of tasks"},
			{Text: "Add", Description: "Add a task"},
			{Text: "Delete", Description: "Delete task"},
			{Text: "Edit", Description: "Edit task by ID"},
			{Text: "Complete", Description: "Complete the task"},
			{Text: "Exit", Description: "Exit"},
		}
	}
	return prompt.FilterHasPrefix(s, d.GetWordBeforeCursor(), true)
}

func executor(s string) {
	texts := strings.Split(strings.ToLower(s), " ")
	switch texts[0] {
	case Show:

		todos, err := serv.ListTodos()
		if err != nil {
			fmt.Println(err)
		}
		for _, todo := range todos {
			fmt.Printf("id:	%10d,Title:	%20s,Description	%30s,Complete	%5t\n", todo.ID, todo.Title, todo.Description, todo.IsComplete)
		}
		fmt.Println()
		return
	case Add:
		reader := bufio.NewReader(os.Stdin)

		fmt.Print("	enter Title :")
		title, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println(err)
			return
		}

		fmt.Print("	enter Description :")
		description, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println(err)
			return
		}

		err = serv.CreateTodo(title, description)
		if err != nil {
			fmt.Println(err)
			return
		}
		return
	case Delete:
		if len(texts) < 2 {
			return
		}
		id, err := strconv.Atoi(texts[1])
		if err != nil {
			fmt.Println(err)
			return
		}
		err = serv.RemoveTodo(id)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println()
		return
	case Edit:
		if len(texts) < 3 {
			return
		}
		reader := bufio.NewReader(os.Stdin)
		if texts[1] == "t" || texts[1] == "title" {
			fmt.Print("	enter Title :")
			title, err := reader.ReadString('\n')
			if err != nil {
				fmt.Println(err)
				return
			}

			id, err := strconv.Atoi(texts[2])
			if err != nil {
				fmt.Println(err)
				return
			}
			err = serv.UpdateTodo(id, title, "")
			if err != nil {
				fmt.Println(err)
				return
			}

			return
		} else if texts[1] == "d" || texts[1] == "description" {
			fmt.Print("	enter Description :")
			description, err := reader.ReadString('\n')
			if err != nil {
				fmt.Println(err)
				return
			}

			id, err := strconv.Atoi(texts[2])
			if err != nil {
				fmt.Println(err)
				return
			}
			err = serv.UpdateTodo(id, "", description)
			if err != nil {
				fmt.Println(err)
				return
			}

			return
		}
	case Complete:
		if len(texts) < 2 {
			return
		}
		id, err := strconv.Atoi(texts[1])
		if err != nil {
			fmt.Println(err)
			return
		}
		err = serv.CompleteTodo(id)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println()
		return
	case Exit:
		os.Exit(0)
		return
	}

}
