package test

import (
	"fmt"
	"testing"

	"gitlab.com/gerzhod/go-kata/module3/clean_architecture/service/service"

	"gitlab.com/gerzhod/go-kata/module3/clean_architecture/service/model"
)

type fackeRepository struct {
	todos []model.Todo
}

func (repo *fackeRepository) GetTodos() ([]model.Todo, error) {
	return repo.todos, nil
}

func (repo *fackeRepository) GetTodo(id int) (model.Todo, error) {
	for _, todo := range repo.todos {
		if id == todo.ID {
			return todo, nil
		}
	}
	return model.Todo{}, fmt.Errorf("todo not found")
}

func (repo *fackeRepository) CreateTodo(task model.Todo) (model.Todo, error) {
	repo.todos = append(repo.todos, task)
	return task, nil
}

func (repo *fackeRepository) UpdateTodo(task model.Todo) (model.Todo, error) {
	for i := 0; i < len(repo.todos); i++ {
		if repo.todos[i].ID == task.ID {
			repo.todos[i] = task
			return task, nil
		}
	}
	return model.Todo{}, fmt.Errorf("todo not found")
}

func (repo *fackeRepository) DeleteTodo(id int) error {
	for i := 0; i < len(repo.todos); i++ {
		if repo.todos[i].ID == id {
			repo.todos[i] = repo.todos[len(repo.todos)-1]
			repo.todos = repo.todos[:len(repo.todos)-1]
			return nil
		}
	}
	return fmt.Errorf("todo not found")
}

func Test_todoService_CompleteTodo(t *testing.T) {
	type fields struct {
		repository fackeRepository
	}
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "empty repository",
			args:    args{todo: model.Todo{ID: 123, Title: "Lorem"}},
			fields:  fields{repository: fackeRepository{}},
			wantErr: true,
		},
		{
			name: "non empty repository/Todo is in the repository",
			args: args{todo: model.Todo{ID: 2, Title: "Lorem2"}},
			fields: fields{
				repository: fackeRepository{
					todos: []model.Todo{{ID: 1, Title: "Lorem1"}, {ID: 2, Title: "Lorem2"}, {ID: 3, Title: "Lorem3"}},
				},
			},
			wantErr: false,
		},
		{
			name: "non empty repository/Todo is not in the repository",
			args: args{todo: model.Todo{ID: 123, Title: "Lorem"}},
			fields: fields{
				repository: fackeRepository{
					todos: []model.Todo{{ID: 1, Title: "Lorem1"}, {ID: 2, Title: "Lorem2"}, {ID: 3, Title: "Lorem3"}},
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.GetService(&tt.fields.repository)
			if err := s.CompleteTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("CompleteTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !tt.wantErr {
				for _, todo := range tt.fields.repository.todos {
					if todo.ID == tt.args.todo.ID {
						if !todo.IsComplete {
							t.Errorf("CompleteTodo() todo = %v, IsComplete %v", todo, todo.IsComplete)
						}
						break
					}
				}
			}
		})
	}
}

func Test_todoService_CreateTodo(t *testing.T) {
	type fields struct {
		repository fackeRepository
	}
	type args struct {
		title string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "empty repository",
			args:    args{title: "Lorem"},
			fields:  fields{repository: fackeRepository{}},
			wantErr: false,
		},
		{
			name: "non empty repository/Todo is in the repository",
			args: args{title: "Lorem"},
			fields: fields{
				repository: fackeRepository{
					todos: []model.Todo{{ID: 1, Title: "Lorem1"}, {ID: 2, Title: "Lorem2"}, {ID: 3, Title: "Lorem3"}},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.GetService(&tt.fields.repository)
			if err := s.CreateTodo(tt.args.title); (err != nil) != tt.wantErr {
				t.Errorf("CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !tt.wantErr {
				found := false
				for _, todo := range tt.fields.repository.todos {
					if todo.Title == tt.args.title {
						found = true
						break
					}
				}
				if !found {
					t.Errorf("CreateTodo() title = %v, not found", tt.args.title)
				}
			}
		})
	}
}

func Test_todoService_ListTodos(t *testing.T) {
	type fields struct {
		repository fackeRepository
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name:    "empty repository",
			fields:  fields{repository: fackeRepository{}},
			wantErr: false,
		},
		{
			name: "non empty repository",
			fields: fields{
				repository: fackeRepository{
					todos: []model.Todo{{ID: 1, Title: "Lorem1"}, {ID: 2, Title: "Lorem2"}, {ID: 3, Title: "Lorem3"}},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.GetService(&tt.fields.repository)
			got, err := s.ListTodos()
			if (err != nil) != tt.wantErr {
				t.Errorf("ListTodos() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				for i := 0; i < len(got); i++ {
					if got[i] != tt.fields.repository.todos[i] {
						t.Errorf("ListTodos() got = %v, want %v", got, tt.fields.repository.todos)
					}
				}
			}
		})
	}
}

func Test_todoService_RemoveTodo(t *testing.T) {
	type fields struct {
		repository fackeRepository
	}
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "empty repository",
			args:    args{todo: model.Todo{ID: 123}},
			fields:  fields{repository: fackeRepository{}},
			wantErr: true,
		},
		{
			name: "non empty repository/Todo is in the repository",
			args: args{todo: model.Todo{ID: 2, Title: "Lorem2"}},
			fields: fields{
				repository: fackeRepository{
					todos: []model.Todo{{ID: 1, Title: "Lorem1"}, {ID: 2, Title: "Lorem2"}, {ID: 3, Title: "Lorem3"}},
				},
			},
			wantErr: false,
		},
		{
			name: "non empty repository/Todo is not in the repository",
			args: args{todo: model.Todo{ID: 123}},
			fields: fields{
				repository: fackeRepository{
					todos: []model.Todo{{ID: 1, Title: "Lorem1"}, {ID: 2, Title: "Lorem2"}, {ID: 3, Title: "Lorem3"}},
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.GetService(&tt.fields.repository)
			if err := s.RemoveTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("RemoveTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !tt.wantErr {
				for i := 0; i < len(tt.fields.repository.todos); i++ {
					if tt.fields.repository.todos[i].ID == tt.args.todo.ID {
						t.Errorf("ListTodos() fieng = %v", tt.args.todo)
					}
				}
			}
		})
	}
}
