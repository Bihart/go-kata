package main

import (
	"fmt"
	"math"
	"testing"
)

type args struct {
	a float64
	b float64
}

var tests = []struct {
	args         args
	wantMultiply float64
	wantDivide   float64
	wantSum      float64
	wantAverage  float64
}{
	{
		args:         args{a: 45, b: 785},
		wantMultiply: 35325,
		wantDivide:   0.05732484076433121,
		wantSum:      830,
		wantAverage:  415,
	},
	{
		args:         args{a: 654, b: 45.4},
		wantMultiply: 29691.6,
		wantDivide:   14.405286343612335,
		wantSum:      699.4,
		wantAverage:  349.7,
	},
	{
		args:         args{a: 0.4, b: 468.4},
		wantMultiply: 187.36,
		wantDivide:   0.0008539709649871905,
		wantSum:      468.79999999999995,
		wantAverage:  234.39999999999998,
	},
	{
		args:         args{a: 0, b: 785},
		wantMultiply: 0,
		wantSum:      785,
		wantAverage:  392.5,
	},
	{
		args:         args{a: 478, b: 0},
		wantMultiply: 0,
		wantDivide:   math.Inf(1),
		wantSum:      478,
		wantAverage:  239,
	},
	{
		args:         args{a: 0, b: 0},
		wantMultiply: 0,
		wantDivide:   math.NaN(),
		wantSum:      0,
		wantAverage:  0,
	},
	{
		args:         args{a: 0.59, b: 0.546},
		wantMultiply: 0.32214,
		wantDivide:   1.0805860805860805,
		wantSum:      1.1360000000000001,
		wantAverage:  0.5680000000000001,
	},
}

func TestMultiply(t *testing.T) {

	for _, tt := range tests {
		t.Run(fmt.Sprintf("Multiply %f and %f", tt.args.a, tt.args.b), func(t *testing.T) {
			if got := multiply(tt.args.a, tt.args.b); got != tt.wantMultiply {
				t.Errorf("multiply() = %v, want %v", got, tt.wantMultiply)
			}
		})
	}
}

func TestDivide(t *testing.T) {

	for _, tt := range tests {
		t.Run(fmt.Sprintf("Divide %f and %f", tt.args.a, tt.args.b), func(t *testing.T) {
			if got := divide(tt.args.a, tt.args.b); got != tt.wantDivide {
				if tt.args.a != 0 || tt.args.b != 0 || !math.IsNaN(got) {
					t.Errorf("divide() = %v, want %v", got, tt.wantDivide)
				}

			}
		})
	}
}

func TestSum(t *testing.T) {

	for _, tt := range tests {
		t.Run(fmt.Sprintf("Sum %f and %f", tt.args.a, tt.args.b), func(t *testing.T) {
			if got := sum(tt.args.a, tt.args.b); got != tt.wantSum {
				t.Errorf("sum() = %v, want %v", got, tt.wantSum)
			}
		})
	}
}

func TestAverage(t *testing.T) {

	for _, tt := range tests {
		t.Run(fmt.Sprintf("Average %f and %f", tt.args.a, tt.args.b), func(t *testing.T) {
			if got := average(tt.args.a, tt.args.b); got != tt.wantAverage {
				t.Errorf("average() = %v, want %v", got, tt.wantAverage)
			}
		})
	}
}
