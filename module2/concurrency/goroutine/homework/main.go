package main

import (
	"fmt"
	"time"
)

func main() {
	mes1 := make(chan string)
	mes2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			mes1 <- "Прошло пол секунды"
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 2)
			mes2 <- "Прошло 2 секунды"
		}
	}()

	for {
		select {
		case mes := <-mes1:
			fmt.Println(mes)
		case mes := <-mes2:
			fmt.Println(mes)

		}
	}
}
