package main

import (
	"fmt"
	"math/rand"

	"github.com/brianvoe/gofakeit/v6"
)

type Person struct {
	Name string
	Age  int
}

func main() {
	p := Person{Name: "Andy", Age: 18}

	// вывод значений структуры
	fmt.Println("simple struct:", p)

	// вывод названий полей и их значений
	fmt.Printf("detailed struct: %+v\n", p)

	// вывод названий полей и их значений в виде инициализации
	fmt.Printf("Golang struct: %#v\n", p)

	for i := 0; i < rand.Intn(900)+100; i++ {
		fmt.Println(generateSelfStory(gofakeit.Name(), rand.Intn(81)+18, float64(rand.Intn(100))+rand.Float64()))
	}

}

func generateSelfStory(name string, age int, money float64) string {
	return fmt.Sprintf("Hello! My name is %s. I'm %d y.o. And I also have $%.2f in my wallet rigt now", name, age, money)
}
