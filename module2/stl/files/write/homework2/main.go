package main

import (
	"os"

	"github.com/alexsergivan/transliterator"
)

// Напиши программу, переводящую все русские символы в транслит, входящий файл example.txt и example.processed.txt на выходе. Текст для файла выбери произвольно.
func main() {

	file, err := os.Open("example.txt")
	check(err)

	fInfo, err := file.Stat()
	check(err)

	fileBytes := make([]byte, fInfo.Size())
	_, err = file.Read(fileBytes)
	check(err)

	check(file.Close())

	file, err = os.Create("example.processed.txt")
	check(err)

	trans := transliterator.NewTransliterator(nil)
	latin := trans.Transliterate(string(fileBytes), "en")

	_, err = file.WriteString(latin)
	check(err)

	check(file.Sync())
	check(file.Close())
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
