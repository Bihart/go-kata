package main

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	dataBase, _ := sql.Open("sqlite3", "module2/stl/database/homework/gopher.db")
	//fmt.Println(dataBase)
	stmt, err := dataBase.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY , firstname TEXT,lastname TEXT)")
	check(err)
	_, err = stmt.Exec()
	check(err)

	stmt, err = dataBase.Prepare("INSERT INTO people (firstname,lastname) VALUES (?,?)")
	check(err)
	_, err = stmt.Exec("Lorem", "Ipsum")
	check(err)

	rows, err := dataBase.Query("SELECT id, firstname, lastname FROM people")
	check(err)
	var id int
	var firstname string
	var lastname string

	for rows.Next() {
		err := rows.Scan(&id, &firstname, &lastname)
		check(err)
		fmt.Printf("%d:	%s %s\n", id, firstname, lastname)
	}
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
