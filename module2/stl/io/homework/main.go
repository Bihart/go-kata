package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

// Создай bytes.Buffer и запиши в него строки, приведенные в коде.
// Создай файл example.txt, запиши строки в файл и помни, что запись данных осуществляется с новой строки.
// Прочти данные файла в новый буфер
// В конце программы выведи данные из нового буфера через fmt.Println.
func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	var buf bytes.Buffer
	// запишите данные в буфер
	for _, text := range data {
		_, err := buf.Write([]byte(text))
		check(err)
		_, err = buf.Write([]byte("\n"))
		check(err)
	}

	// создайте файл
	file, err := os.Create("example.txt")
	check(err)

	// запишите данные в файл
	_, err = file.Write(buf.Bytes())
	check(err)
	check(file.Close())

	// прочтите данные в новый буфе
	file, err = os.Open("example.txt")
	check(err)
	var buf2 bytes.Buffer
	_, err = io.Copy(&buf2, file)
	check(err)

	check(file.Close())

	fmt.Println(buf2.String()) // выведите данные из буфера buffer.String()
	// у вас все получится!
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
