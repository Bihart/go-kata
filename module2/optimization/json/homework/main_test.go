package main

import (
	"testing"
)

func BenchmarkStandardJsonUnmarshalWelcome(b *testing.B) {
	var (
		jsonData = []byte("[\n  {\n    \"id\": 0,\n    \"category\": {\n      \"id\": 0,\n      \"name\": \"string\"\n    },\n    \"name\": \"doggie\",\n    \"photoUrls\": [\n      \"string\"\n    ],\n    \"tags\": [\n      {\n        \"id\": 0,\n        \"name\": \"string\"\n      }\n    ],\n    \"status\": \"available\"\n  }\n]")
		welcome  Welcome
		err      error
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		welcome, err = UnmarshalWelcome(jsonData)
		if err != nil {
			panic(err)
		}
		_ = welcome
	}
}

func BenchmarkStandardJsonMarshal(b *testing.B) {
	var (
		welcome Welcome = Welcome{
			WelcomeElement{
				ID:        0,
				Status:    "available",
				Category:  Category{ID: 0, Name: "string"},
				Name:      "string",
				PhotoUrls: []string{"string"},
				Tags:      []Category{{ID: 0, Name: "string"}},
			},
		}
		err  error
		data []byte
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = welcome.Marshal()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

func BenchmarkJsonIterUnmarshalWelcome(b *testing.B) {
	var (
		jsonData = []byte("[\n  {\n    \"id\": 0,\n    \"category\": {\n      \"id\": 0,\n      \"name\": \"string\"\n    },\n    \"name\": \"doggie\",\n    \"photoUrls\": [\n      \"string\"\n    ],\n    \"tags\": [\n      {\n        \"id\": 0,\n        \"name\": \"string\"\n      }\n    ],\n    \"status\": \"available\"\n  }\n]")
		welcome  Welcome
		err      error
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		welcome, err = UnmarshalWelcome2(jsonData)
		if err != nil {
			panic(err)
		}
		_ = welcome
	}
}
func BenchmarkJsonIterMarshal(b *testing.B) {
	var (
		welcome Welcome = Welcome{
			WelcomeElement{
				ID:        0,
				Status:    "available",
				Category:  Category{ID: 0, Name: "string"},
				Name:      "string",
				PhotoUrls: []string{"string"},
				Tags:      []Category{{ID: 0, Name: "string"}},
			},
		}
		err  error
		data []byte
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = welcome.Marshal2()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}
