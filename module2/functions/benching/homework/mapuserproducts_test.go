package main

import (
	"testing"
)

func BenchmarkTestMapUserProducts(b *testing.B) {
	var (
		users    = genUsers()
		products = genProducts()
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkTestMapUserProducts2(b *testing.B) {
	var (
		users    = genUsers()
		products = genProducts()
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}

}
