package main

import (
	"errors"
	"fmt"
)

//реализуй функцию ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error), которая выполняет джобу MergeDictsJob и возвращает ее.
//
//	Алгоритм обработки джобы следующий:
//перебрать по порядку все словари job.Dicts и записать каждое ключ-значение в результирующую мапу job.Merged;
//если в структуре job.Dicts меньше двух словарей, возвращается ошибка errNotEnoughDicts = errors.New(at least 2 dictionaries are required);
//если в структуре job.Dicts встречается словарь в виде нулевого значения nil, то возвращается ошибка errNilDict = errors.New(nil dictionary);
//независимо от успешного выполнения или ошибки в возвращаемой структуре MergeDictsJob, поле IsFinished должно быть заполнено как true.

var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func main() {
	mergeDictsJob := MergeDictsJob{Dicts: []map[string]string{
		map[string]string{
			"1_1": "3w4f5",
			"1_2": "23d35d",
			"1_3": "23f5",
			"1_4": "32d54",
		},
		map[string]string{
			"2_1": "h457",
			"2_2": "45g47g",
			"2_3": "45gf4g7",
			"2_4": "45gf67",
			"2_5": "vy54v4t4",
		},
	}}
	fmt.Println(mergeDictsJob)
	var job, err = ExecuteMergeDictsJob(&mergeDictsJob)
	if err != nil {
		println(err)
	}

	fmt.Println(*job)

}

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	if len(job.Dicts) < 2 {
		job.IsFinished = true
		return job, errNotEnoughDicts
	}

	job.Merged = map[string]string{}
	for _, dict := range job.Dicts {
		if dict == nil {
			job.IsFinished = true
			return job, errNilDict
		}

		for i, s := range dict {
			job.Merged[i] = s
		}
	}
	job.IsFinished = true
	return job, nil

}
