// You can edit this code!
// Click here and start typing.
package main

import "fmt"

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() { // Исправь 22 строку программы, чтобы программа вывела Success!.
	var i Userer //nolint:all
	i = &User{}
	_ = i
	fmt.Println("Success!")
}
