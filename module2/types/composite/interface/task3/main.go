// Исправь 37 строку программы, чтобы распечатать имена пользователей. Внимание: можно удалить 37 строку, дописать рабочий код, не модифицируя функцию testUserName.
package main

import (
	"fmt"
)

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string {
	return u.Name

}

type Userer interface {
	GetName() string
}

func main() {
	u := []User{
		{
			ID:   34,
			Name: "Annet",
		},
		{
			ID:   55,
			Name: "John",
		},
		{
			ID:   89,
			Name: "Alex",
		},
	}
	var users []Userer
	for i := 0; i < len(u); i++ {
		users = append(users, &u[i])
	}
	testUserName(users)
}

func testUserName(users []Userer) {
	for _, u := range users {
		fmt.Println(u.GetName())
	}
}
