package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/malgamves/CommunityWriterPrograms",
			Stars: 2821,
		},
		{
			Name:  "https://github.com/codecrafters-io/build-your-own-x",
			Stars: 188000,
		},
		{
			Name:  "https://github.com/ossu/computer-science",
			Stars: 134000,
		},
		{
			Name:  "https://github.com/vinta/awesome-python",
			Stars: 156000,
		},
		{
			Name:  "https://github.com/public-apis/public-apis",
			Stars: 227000,
		},
		{
			Name:  "https://github.com/yangshun/tech-interview-handbook",
			Stars: 86000,
		},
		{
			Name:  "https://github.com/30-seconds/30-seconds-of-code",
			Stars: 107000,
		},
		{
			Name:  "https://github.com/hakimel/reveal.js",
			Stars: 63100,
		},
		{
			Name:  "https://github.com/EbookFoundation/free-programming-books",
			Stars: 266000,
		},
		{
			Name:  "https://github.com/trekhleb/javascript-algorithms",
			Stars: 163000,
		},
		{
			Name:  "https://github.com/kamranahmedse/developer-roadmap",
			Stars: 228000,
		},
		{
			Name:  "https://github.com/jwasham/coding-interview-university",
			Stars: 246000,
		},
	}

	projectsMap := make(map[string]Project)

	for _, project := range projects {
		projectsMap[project.Name] = project
	}

	for project := range projectsMap {
		fmt.Println(project)
	}

	// в цикле пройдитесь по мапе и выведите значения в консоль
}
