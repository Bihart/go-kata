package main

import (
	"fmt"
)

type User struct {
	Name string
	Age  int
}

// Удали пользователей возрастом выше 40 лет из слайса, выведи результат в консоль
func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}

	removeOld(&users)

	fmt.Println(users)
}

func removeOld(users *[]User) {
	temp := *users
	for i := 0; i < len(temp); i++ {
		if temp[i].Age > 40 {
			temp = append(temp[:i], temp[i+1:]...)
			i--
		}
	}
	*users = temp

	/*var newUsers []User
	for _, user := range *users {
		if user.Age <= 40 {
			newUsers = append(newUsers, user)
		}
	}
	*users = newUsers*/
}
