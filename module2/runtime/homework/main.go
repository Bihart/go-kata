// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
	"runtime"
)

//Исправь программу так, чтобы выполнились 2 fmt.Println в функции main без использования блокировок, например,
//time.Sleep, и конструкций синхронизации, используя функцию runtime.Gosched().

func main() {
	fmt.Println("i can manage")
	go func() {
		fmt.Println("goroutines in Golang!")
	}()
	runtime.Gosched()
	fmt.Println("and its awesome!")
}
