module gitlab.com/gerzhod/go-kata

go 1.19

require (
	github.com/alexsergivan/transliterator v1.0.0
	github.com/brianvoe/gofakeit/v6 v6.20.2
	github.com/c-bata/go-prompt v0.2.6
	github.com/go-chi/chi/v5 v5.0.8
	github.com/json-iterator/go v1.1.12
	github.com/mattn/go-sqlite3 v1.14.16
	gitlab.com/gerzhod/greet v0.0.0-20230210020739-5696e8150455
	gopkg.in/loremipsum.v1 v1.1.2
)

require (
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mattn/go-tty v0.0.3 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pkg/term v1.2.0-beta.2 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	golang.org/x/sys v0.0.0-20200918174421-af09f7315aff // indirect
)
